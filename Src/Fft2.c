#include "FftCore.h"
#include "Fft2.h"
#include <stdlib.h>


/*--- fft2 --- ２次元ＦＦＴの実行 ---------------------------------------------
                （X_SIZE，Y_SIZEが２のべき乗の場合に限る）
        a_rl:	データ実数部（入出力兼用）
        a_im:	データ虚数部（入出力兼用）
        inv:	1: ＤＦＴ，-1: 逆ＤＦＴ
-----------------------------------------------------------------------------*/
int fft2(float a_rl[Y_SIZE][X_SIZE], float a_im[Y_SIZE][X_SIZE], int inv) {
  float *b_rl;     /* データ転置作業用配列（実数部）*/
  float *b_im;     /* データ転置作業用配列（虚数部）*/
  float *hsin_tbl; /* 水平用SIN計算用テーブル		*/
  float *hcos_tbl; /* 水平用COS計算用テーブル		*/
  float *vsin_tbl; /* 垂直用SIN計算用テーブル		*/
  float *vcos_tbl; /* 垂直用COS計算用テーブル		*/
  float *buf_x;    /* 作業用バッファ（水平方向）	*/
  float *buf_y;    /* 作業用バッファ（垂直方向）	*/
  int i;

  b_rl = (float *)calloc((size_t)X_SIZE * Y_SIZE, sizeof(float));
  b_im = (float *)calloc((size_t)X_SIZE * Y_SIZE, sizeof(float));
  hsin_tbl = (float *)calloc((size_t)X_SIZE, sizeof(float));
  hcos_tbl = (float *)calloc((size_t)X_SIZE, sizeof(float));
  vsin_tbl = (float *)calloc((size_t)Y_SIZE, sizeof(float));
  vcos_tbl = (float *)calloc((size_t)Y_SIZE, sizeof(float));
  buf_x = (float *)malloc((size_t)X_SIZE * sizeof(float));
  buf_y = (float *)malloc((size_t)Y_SIZE * sizeof(float));
  if ((b_rl == NULL) || (b_im == NULL) || (hsin_tbl == NULL) ||
      (hcos_tbl == NULL) || (vsin_tbl == NULL) || (vcos_tbl == NULL) ||
      (buf_x == NULL) || (buf_y == NULL)) {
    return -1;
  }
  cstb(X_SIZE, inv, hsin_tbl, hcos_tbl); /* 水平用SIN,COSテーブル作成	*/
  cstb(Y_SIZE, inv, vsin_tbl, vcos_tbl); /* 垂直用SIN,COSテーブル作成	*/
  /* 水平方向のＦＦＴ */
  for (i = 0; i < Y_SIZE; i++) {
    fftcore(&a_rl[(long)i][0], &a_im[(long)i][0], X_SIZE, X_EXP, hsin_tbl,
             hcos_tbl, buf_x);
  }
  /* ２次元データの転置 */
  rvmtx1((float(*)[X_SIZE])a_rl, (float(*)[X_SIZE])b_rl, X_SIZE, Y_SIZE);
  rvmtx1((float(*)[X_SIZE])a_im, (float(*)[X_SIZE])b_im, X_SIZE, Y_SIZE);
  /* 垂直方向のＦＦＴ */
  for (i = 0; i < X_SIZE; i++) {
    fftcore(&b_rl[(long)Y_SIZE * i], &b_im[(long)Y_SIZE * i], Y_SIZE, Y_EXP,
             vsin_tbl, vcos_tbl, buf_y);
  }
  /* ２次元データの転置 */
  rvmtx2((float(*)[Y_SIZE])b_rl, (float(*)[Y_SIZE])a_rl, X_SIZE, Y_SIZE);
  rvmtx2((float(*)[Y_SIZE])b_im, (float(*)[Y_SIZE])a_im, X_SIZE, Y_SIZE);
  free(b_rl);
  free(b_im);
  free(hsin_tbl);
  free(hcos_tbl);
  free(vsin_tbl);
  free(vcos_tbl);
  return 0;
}

