#ifndef __INCLUDE_SEXTRACT_H__
#define __INCLUDE_SEXTRACT_H__

#include "Params.h"

void size_extract(unsigned char image_label_in[Y_SIZE][X_SIZE],
                  unsigned char image_label_out[Y_SIZE][X_SIZE], int cnt,
                  float size[], float size_min, float size_max);

#endif
