#ifndef __INCLUDE_RGB_YC_H__
#define __INCLUDE_RGB_YC_H__

#include "Params.h"

void rgb_to_yc(const unsigned char image_in[3][Y_SIZE][X_SIZE], int y[Y_SIZE][X_SIZE],
               int c1[Y_SIZE][X_SIZE], int c2[Y_SIZE][X_SIZE]);

void yc_to_rgb(const int y[Y_SIZE][X_SIZE], const int c1[Y_SIZE][X_SIZE],
               const int c2[Y_SIZE][X_SIZE], unsigned char image_out[3][Y_SIZE][X_SIZE]);

#endif
