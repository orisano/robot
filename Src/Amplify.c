#include "Amplify.h"

/*--- amplify --- 画像の明るさをｎ倍する --------------------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        n:			倍率
-----------------------------------------------------------------------------*/
void amplify(unsigned char image_in[Y_SIZE][X_SIZE],
             unsigned char image_out[Y_SIZE][X_SIZE], int n) {
  int i, j, nf;

  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      nf = (int)image_in[i][j] * n;
      if (nf > 255) nf = 255;
      image_out[i][j] = (unsigned char)nf;
    }
  }
}
