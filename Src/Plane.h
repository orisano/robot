#ifndef __INCLUDE_PLANE_H__
#define __INCLUDE_PLANE_H__

#include "Params.h"

void plane(unsigned char image_in[Y_SIZE][X_SIZE],
           unsigned char image_out[Y_SIZE][X_SIZE],
           unsigned char image_buf[Y_SIZE][X_SIZE], long hist[256]);

#endif
