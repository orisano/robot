#ifndef __INCLUDE_PARAM_H__
#define __INCLUDE_PARAM_H__

#define X_SIZE (256) /* 画像の横方向の大きさ		*/
#define Y_SIZE (256) /* 画像の縦方向の大きさ		*/
#define X_EXP (7)    /* X_EXP = log2(X_SIZE)		*/
#define Y_EXP (7)    /* Y_EXP = log2(Y_SIZE)		*/
#define HIGH (255)   /* ２値画像のハイ・レベル	*/
#define LOW (0)      /* ２値画像のロー・レベル	*/
#define IMAGE_R (0)
#define IMAGE_G (1)
#define IMAGE_B (2)

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327)
#endif

#define OFF_DEMO 1 /* １：開発時　　０：DEMO時*/
#define ON_DEMO (!OFF_DEMO)

#endif
