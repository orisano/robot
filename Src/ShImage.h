#ifndef __INCLUDE_SH_IMAGE_H__
#define __INCLUDE_SH_IMAGE_H__

#include "Params.h"

int sat_image(int sat[Y_SIZE][X_SIZE], unsigned char image_out[Y_SIZE][X_SIZE]);
void hue_image(int sat[Y_SIZE][X_SIZE], int hue[Y_SIZE][X_SIZE], float stdsat,
               unsigned char image_out[Y_SIZE][X_SIZE]);

#endif
