#ifndef __INCLUDE_IMAGE_COM_H__
#define __INCLUDE_IMAGE_COM_H__

#include "Params.h"

void image_copy(unsigned char image_in[Y_SIZE][X_SIZE], unsigned char image_out[Y_SIZE][X_SIZE]);
void image_copy_color(unsigned char image_in[3][Y_SIZE][X_SIZE], unsigned char image_out[3][Y_SIZE][X_SIZE]);
void image_clear(unsigned char image[Y_SIZE][X_SIZE]);
void image_clear_color(unsigned char image[3][Y_SIZE][X_SIZE]);

#endif
