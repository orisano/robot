#ifndef __INCLUDE_DPCM_H__
#define __INCLUDE_DPCM_H__

#include "Params.h"

void dpcm1(unsigned char image_in[Y_SIZE][X_SIZE], int line,
           short data_out[X_SIZE]);
void dpcm2(unsigned char image_in[Y_SIZE][X_SIZE], int line,
           short data_out[X_SIZE]);

#endif
