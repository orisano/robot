#ifndef __INCLUDE_DISK_H__
#define __INCLUDE_DISK_H__

#include "Params.h"

int image_read(unsigned char image[Y_SIZE][X_SIZE], char *filename);
int image_write(unsigned char image[Y_SIZE][X_SIZE], char *filename);
int image_read_color(unsigned char image[3][Y_SIZE][X_SIZE], char *filename);
int image_write_color(unsigned char image[3][Y_SIZE][X_SIZE], char *filename);


#endif
