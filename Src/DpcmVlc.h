#ifndef __INCLUDE_DPCM_VLC_H__
#define __INCLUDE_DPCM_VLC_H__

#include "Params.h"

int dpcm_vlcode(unsigned char image_in[Y_SIZE][X_SIZE], unsigned char image_buf[]);

#endif
