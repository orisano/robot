#ifndef __INCLUDE_SCALE_H__
#define __INCLUDE_SCALE_H__

#include "Params.h"

void scale(unsigned char image_in[Y_SIZE][X_SIZE],
           unsigned char image_out[Y_SIZE][X_SIZE], float zx, float zy);

#endif
