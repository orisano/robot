#ifndef __INCLUDE_IDPCM_VLC_H__
#define __INCLUDE_IDPCM_VLC_H__

#include "Params.h"

int idpcm_vlcode(unsigned char image_buf[],
                 unsigned char image_out[Y_SIZE][X_SIZE]);


#endif
