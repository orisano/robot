#ifndef __INCLUDE_EXPAND_H__
#define __INCLUDE_EXPAND_H__

#include "Params.h"

void expand(unsigned char image_in[Y_SIZE][X_SIZE],
            unsigned char image_out[Y_SIZE][X_SIZE], int fmax, int fmin);

#endif
