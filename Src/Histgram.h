#ifndef __INCLUDE_HISTGRAM_H__
#define __INCLUDE_HISTGRAM_H__

#include "Params.h"

void histgram(unsigned char image_in[Y_SIZE][X_SIZE], long hist[256]);

#endif
