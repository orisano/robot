#ifndef __INCLUDE_FFT_FILT_H__
#define __INCLUDE_FFT_FILT_H__

#include "Params.h"

int fftfilter(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_out[Y_SIZE][X_SIZE], int a, int b);

#endif
