#include "Synth.h"

/*--- synth --- クロマキーによる画面合成 --------------------------------------
        image_in1_r:		入力前景Ｒ画像
        image_in1_g:		入力前景Ｇ画像
        image_in1_b:		入力前景Ｂ画像
        image_in2_r:		入力背景Ｒ画像
        image_in2_g:		入力背景Ｇ画像
        image_in2_b:		入力背景Ｂ画像
        image_out_r:		出力合成Ｒ画像
        image_out_g:		出力合成Ｇ画像
        image_out_b:		出力合成Ｂ画像
        image_key:			合成用キー画像
-----------------------------------------------------------------------------*/
void synth(unsigned char image_in1_r[Y_SIZE][X_SIZE],
           unsigned char image_in1_g[Y_SIZE][X_SIZE],
           unsigned char image_in1_b[Y_SIZE][X_SIZE],
           unsigned char image_in2_r[Y_SIZE][X_SIZE],
           unsigned char image_in2_g[Y_SIZE][X_SIZE],
           unsigned char image_in2_b[Y_SIZE][X_SIZE],
           unsigned char image_out_r[Y_SIZE][X_SIZE],
           unsigned char image_out_g[Y_SIZE][X_SIZE],
           unsigned char image_out_b[Y_SIZE][X_SIZE],
           unsigned char image_key[Y_SIZE][X_SIZE]) {
  int i, j;
  int rr1, gg1, bb1;
  int rr2, gg2, bb2;
  long kk, dyn;

  dyn = HIGH - LOW;
  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      rr1 = (int)image_in1_r[i][j];
      gg1 = (int)image_in1_g[i][j];
      bb1 = (int)image_in1_b[i][j];
      rr2 = (int)image_in2_r[i][j];
      gg2 = (int)image_in2_g[i][j];
      bb2 = (int)image_in2_b[i][j];
      kk = (long)image_key[i][j];
      image_out_r[i][j] = (unsigned char)((rr1 * kk + rr2 * (HIGH - kk)) / dyn);
      image_out_g[i][j] = (unsigned char)((gg1 * kk + gg2 * (HIGH - kk)) / dyn);
      image_out_b[i][j] = (unsigned char)((bb1 * kk + bb2 * (HIGH - kk)) / dyn);
    }
  }
}
