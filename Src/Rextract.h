#ifndef __INCLUDE_REXTRACT_H__
#define __INCLUDE_REXTRACT_H__

#include "Params.h"

void ratio_extract(unsigned char image_label_in[Y_SIZE][X_SIZE],
                   unsigned char image_label_out[Y_SIZE][X_SIZE], int cnt,
                   float ratio[], float ratio_min, float ratio_max);

#endif
