#include "ThreshC.h"

/*--- thresh_color --- R,G,B値による閾値処理 ----------------------------------
        image_in_r:		入力Ｒ画像
        image_in_g:		入力Ｇ画像
        image_in_b:		入力Ｂ画像
        image_out_r:	出力Ｒ画像
        image_out_g:	出力Ｇ画像
        image_out_b:	出力Ｂ画像
        thdrl, thdrm:	Ｒの閾値 (min,max)
        thdgl, thdgm:	Ｇの閾値 (min,max)
        thdbl, thdbm:	Ｂの閾値 (min,max)
-----------------------------------------------------------------------------*/
void thresh_color(unsigned char image_in_r[Y_SIZE][X_SIZE],
                  unsigned char image_in_g[Y_SIZE][X_SIZE],
                  unsigned char image_in_b[Y_SIZE][X_SIZE],
                  unsigned char image_out_r[Y_SIZE][X_SIZE],
                  unsigned char image_out_g[Y_SIZE][X_SIZE],
                  unsigned char image_out_b[Y_SIZE][X_SIZE], int thdrl,
                  int thdrm, int thdgl, int thdgm, int thdbl, int thdbm) {
  int i, j;

  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      image_out_r[i][j] = image_in_r[i][j];
      image_out_g[i][j] = image_in_g[i][j];
      image_out_b[i][j] = image_in_b[i][j];
    }
  }
  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      if (image_out_r[i][j] < thdrl)
        image_out_r[i][j] = image_out_g[i][j] = image_out_b[i][j] = 0;
      if (image_out_r[i][j] > thdrm)
        image_out_r[i][j] = image_out_g[i][j] = image_out_b[i][j] = 0;
      if (image_out_g[i][j] < thdgl)
        image_out_r[i][j] = image_out_g[i][j] = image_out_b[i][j] = 0;
      if (image_out_g[i][j] > thdgm)
        image_out_r[i][j] = image_out_g[i][j] = image_out_b[i][j] = 0;
      if (image_out_b[i][j] < thdbl)
        image_out_r[i][j] = image_out_g[i][j] = image_out_b[i][j] = 0;
      if (image_out_b[i][j] > thdbm)
        image_out_r[i][j] = image_out_g[i][j] = image_out_b[i][j] = 0;
    }
  }
}
