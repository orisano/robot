#ifndef __INCLUDE_HIST_IMAG_H__
#define __INCLUDE_HIST_IMAG_H__

#include "Params.h"

void histimage(long hist[256], unsigned char image_hist[Y_SIZE][X_SIZE]);

#endif
