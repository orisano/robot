#ifndef __INCLUDE_SOFT_MASK_H__
#define __INCLUDE_SOFT_MASK_H__

#include "Params.h"

void soft_mask(unsigned char image_in_r[Y_SIZE][X_SIZE],
               unsigned char image_in_g[Y_SIZE][X_SIZE],
               unsigned char image_in_b[Y_SIZE][X_SIZE],
               unsigned char image_key[Y_SIZE][X_SIZE], int thdh, int thdl);

#endif
