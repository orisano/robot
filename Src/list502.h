//
//	list502.h
//
#include <Windows.h>
#include <vfw.h>

// グラフィック用ウィンドウのデータをまとめた構造体
typedef struct {
  HINSTANCE hi;
  int x;
  int y;
  HWND hwnd;
  BYTE *lpBmpData;
  BITMAPINFOHEADER bih;
} IMG0;

int gr_reg(void);
void gr_init(IMG0 *);
DWORD th_Proc(void *);
LRESULT CALLBACK grProc(HWND, UINT, WPARAM, LPARAM);
