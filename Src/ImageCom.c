#include "ImageCom.h"
#include <string.h>

/*--- image_copy --- 画像データをコピーする
-----------------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
-----------------------------------------------------------------------------*/
void image_copy(unsigned char image_in[Y_SIZE][X_SIZE],
                unsigned char image_out[Y_SIZE][X_SIZE]) {
  memcpy(image_out, image_in, Y_SIZE * X_SIZE);
}

/*--- image_copy_color ---
画像データをコピーする（カラー）--------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
-----------------------------------------------------------------------------*/
void image_copy_color(unsigned char image_in[3][Y_SIZE][X_SIZE],
                      unsigned char image_out[3][Y_SIZE][X_SIZE]) {
  memcpy(image_out, image_in, 3 * Y_SIZE * X_SIZE);
}

/*--- image_clear --- モノクロ画像データをクリアする
--------------------------
        image:	入力画像配列
-----------------------------------------------------------------------------*/
void image_clear(unsigned char image[Y_SIZE][X_SIZE]) {
  memset(image, 0, Y_SIZE * X_SIZE);
}

/*--- image_clear_color --- カラー画像データをクリアする
----------------------
        image:	入力画像配列
-----------------------------------------------------------------------------*/
void image_clear_color(unsigned char image[3][Y_SIZE][X_SIZE]) {
  memset(image, 0, 3 * Y_SIZE * X_SIZE);
}
