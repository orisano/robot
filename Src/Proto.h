#ifndef __INCLUDE_PROTO_H__
#define __INCLUDE_PROTO_H__

#include "WinParams.h"
#include "WinCImage.h"
#include "WinProto.h"

#if ON_DEMO
#include "RobotAgent.h"
#endif

#include <windows.h>
#include <dshow.h>
#include <stdio.h>
#include <conio.h>

#include <qedit.h>

#include "Affine.h"
#include "Amplify.h"
#include "CSh.h"
#include "Colorbar.h"
#include "Dilation.h"
#include "Disk.h"
#include "Dpcm.h"
#include "DpcmVlc.h"
#include "Event.h"
#include "Vlcode.h"
#include "Erosion.h"
#include "Expand.h"
#include "Fft1.h"
#include "Fft2.h"
#include "FftFilt.h"
#include "FftImage.h"
#include "cam_usb_std_x.h"
#include "Features.h"
#include "ImageCom.h"
#include "Labeling.h"
#include "Gradient.h"
#include "HardMask.h"
#include "Hist2Im.h"
#include "HistImag.h"
#include "HistPrin.h"
#include "HistSmth.h"
#include "Histgram.h"
#include "Idpcm.h"
#include "Ivlcode.h"
#include "IdpcmVlc.h"
#include "ImageTra.h"
#include "Laplacia.h"
#include "Masking.h"
#include "Median.h"
#include "Perspect.h"
#include "Plane.h"
#include "Range.h"
#include "Rextract.h"
#include "RgbYc.h"
#include "Rotation.h"
#include "Scale.h"
#include "ScaleNG.h"
#include "ScaleNea.h"
#include "Sextract.h"
#include "ShImage.h"
#include "Shift.h"
#include "Smooth.h"
#include "SoftMask.h"
#include "Ssynth.h"
#include "Synth.h"
#include "Template.h"
#include "Thinning.h"
#include "Thresh.h"
#include "ThreshC.h"
#include "TranYsh.h"

extern char *wTitle[4]; /* = {"IN", "OUT", "WORK", "KEY"}; /*
                           ウィンドウのタイトル	*/
extern int bitspixel; /*  一画素当たりのビット数	*/
extern unsigned char
    image_in[3][Y_SIZE][X_SIZE]; /*  入力画像配列 */
extern unsigned char
    image_out[3][Y_SIZE][X_SIZE]; /*  出力画像配列 */
extern unsigned char
    image_work[3][Y_SIZE][X_SIZE]; /*  ワーク画像配列 */
extern unsigned char
    image_key[3][Y_SIZE][X_SIZE]; /*　キー画像配列 */
extern unsigned char
    image_buf[3][Y_SIZE][X_SIZE]; /*　バッファ配列     		*/
#ifdef USE_PRINT
extern unsigned char image_prt1
    [P_Y_SIZE][P_X_SIZE]; /*　印刷用画像配列     		*/
extern unsigned char image_prt2
    [P_Y_SIZE][P_X_SIZE]; /*　印刷用画像配列     		*/
#endif
extern unsigned char
    image_black[Y_SIZE][X_SIZE];              /* 黒の画像配列     	*/
extern unsigned char image_r[Y_SIZE][X_SIZE]; /*　画像配列（R) */
extern unsigned char image_g[Y_SIZE][X_SIZE]; /*　画像配列（G) */
extern unsigned char image_b[Y_SIZE][X_SIZE]; /*　画像配列（B) */
extern unsigned char
    image_hist[Y_SIZE][X_SIZE]; /*　ヒスト画像配列 */
extern int y[Y_SIZE][X_SIZE];       /* Ｙのデータ配列　     */
extern int c1[Y_SIZE][X_SIZE];      /* Ｒ−Ｙのデータ配列   */
extern int c2[Y_SIZE][X_SIZE];      /* Ｂ−Ｙのデータ配列   */
extern int sat[Y_SIZE][X_SIZE];     /* 彩度のデータ配列     */
extern int hue[Y_SIZE][X_SIZE];     /* 色相のデータ配列     */
extern long hist[256];              /* ヒストグラム配列 	*/
extern float ratio[128], size[128]; /* 特徴パラメータ配列   */

#if OFF_DEMO
extern HANDLE ghInst;        /*　インスタンスハンドル	*/
extern HWND hGWnd;           /*　メインウィンドウ		*/
extern HBITMAP hBitmap;      /*　ビットマップ			*/
extern HMENU hSubMenu;       /*　サブメニュー			*/
extern ImageWin imageWin[4]; /*　画像用ウィンドウ		*/
/* コモンダイアログで必要な変数 */
extern OPENFILENAME ofn;
extern char szFilterSpec
    [128]; /* ファイルの種類を指定するフィルタ	*/
extern char szFileName
    [MAX_NAME_LENGTH]; /*　ファイル名（パス付き）	*/
extern char szFileTitle
    [MAX_NAME_LENGTH]; /*　ファイル名（パス無し）	*/
#endif

//  2004.9.9.--9.25.--10.5.
extern int flow_point[300][4];
extern int flow_point_size, flow_limit;
extern int t_max, t_min, i_max, i_min;
extern int t_sx, t_sy, t_ex, t_ey, tx, ty;
extern int x_panorama, y_panorama;
extern int man_way_TBL[256];
extern int man_way, man_direct;

extern unsigned char imgbuff[X_SIZE * Y_SIZE];

extern int omni_man_way, cam_man_dist;
//距離　　単位mm　　０〜５０００
extern int omni_man_direct, cam_man_dir;
//方角　　°　０〜３６０
extern int omni_accord, cam_man_accord;
//一致度
extern int omni_time, cam_man_time;

// 2008.9.11.
// extern DWORD Kick(int iID, int iSw);				//
// キックデバイス(0:停止、1:通電)
extern unsigned long rbInit();
extern unsigned long rbExit();
extern unsigned long rbStop(int iID);
extern unsigned long rbFor(int iID, int iDist);
extern unsigned long rbBack(int iID, int iDist);
extern unsigned long rbTurnRight(int iID, int iAngle);
extern unsigned long rbTurnLeft(int iID, int iAngle);
extern unsigned long rbKickOn(int iID);
extern unsigned long rbKickOff(int iID);

#if ON_DEMO
extern RobotAgent ra;
#endif

#endif
