#ifndef __INCLUDE_COLORBAR_H__
#define __INCLUDE_COLORBAR_H__

#include "Params.h"

void colorbar(unsigned char image_r[Y_SIZE][X_SIZE],
              unsigned char image_g[Y_SIZE][X_SIZE],
              unsigned char image_b[Y_SIZE][X_SIZE], int level);

#endif
