#include "params.h"
#include "Proto.h"

#if OFF_DEMO
#include "WinParams.h"
#include "WinCImage.h"
#include "WinProto.h"
#endif

char *wTitle[4] = {"IN 入力画像", "OUT 出力画像", "WORK",
                   "KEY"};                   /*  ウィンドウのタイトル	*/
int bitspixel;                               /*  一画素当たりのビット数	*/
unsigned char image_in[3][Y_SIZE][X_SIZE];   /*  入力画像配列 */
unsigned char image_out[3][Y_SIZE][X_SIZE];  /*  出力画像配列 */
unsigned char image_work[3][Y_SIZE][X_SIZE]; /*  ワーク画像配列      	*/
unsigned char image_key[3][Y_SIZE][X_SIZE];  /*　キー画像配列 */
unsigned char image_buf[3][Y_SIZE][X_SIZE];  /*　バッファ配列 */
#ifdef USE_PRINT
unsigned char image_prt1[P_Y_SIZE][P_X_SIZE]; /*　印刷用画像配列 */
unsigned char image_prt2[P_Y_SIZE][P_X_SIZE]; /*　印刷用画像配列 */
#endif
unsigned char image_black[Y_SIZE][X_SIZE]; /* 黒の画像配列     	*/
unsigned char image_r[Y_SIZE][X_SIZE];     /*　画像配列（R)     		*/
unsigned char image_g[Y_SIZE][X_SIZE];     /*　画像配列（G)     		*/
unsigned char image_b[Y_SIZE][X_SIZE];     /*　画像配列（B)    		*/
unsigned char image_hist[Y_SIZE][X_SIZE];  /*　ヒスト画像配列 */
int y[Y_SIZE][X_SIZE];                     /* Ｙのデータ配列　     */
int c1[Y_SIZE][X_SIZE];                    /* Ｒ−Ｙのデータ配列   */
int c2[Y_SIZE][X_SIZE];                    /* Ｂ−Ｙのデータ配列   */
int sat[Y_SIZE][X_SIZE];                   /* 彩度のデータ配列     */
int hue[Y_SIZE][X_SIZE];                   /* 色相のデータ配列     */
long hist[256];                            /* ヒストグラム配列 	*/
float ratio[128], size[128];               /* 特徴パラメータ配列   */

#if OFF_DEMO
HANDLE ghInst;        /*　インスタンスハンドル	*/
HWND hGWnd;           /*　メインウィンドウ		*/
HBITMAP hBitmap;      /*　ビットマップ			*/
HMENU hSubMenu;       /*　サブメニュー			*/
ImageWin imageWin[4]; /*　画像用ウィンドウ		*/

/* コモンダイアログで必要な変数 */
OPENFILENAME ofn;
char szFilterSpec[128] = /* ファイルの種類を指定するフィルタ	*/
    "カラー画像ファイル (*.bmp)\0*.bmp\0";
char szFileName[MAX_NAME_LENGTH];  /*　ファイル名（パス付き）	*/
char szFileTitle[MAX_NAME_LENGTH]; /*　ファイル名（パス無し）	*/
#endif

//  2004.9.9.--9.25.--10.5.
int flow_point[300][4];
int flow_point_size, flow_limit;
int t_max, t_min, i_max, i_min;
int t_sx, t_sy, t_ex, t_ey, tx, ty;
int x_panorama, y_panorama;
int man_way_TBL[256];
int man_way, man_direct;

/* カメラ映像ID */
int cameraid;
/* 映像取得ID */
int imggetid;
/* 表示画像ID */
int dispid;
/* テンプレート画像ID */
// int templateid;
/* ワーク画像ID */
int workid;

/*20080311
IMGID img_cur,img_prev,img_sub;
//2004.10.21.--10.22.
IMGID ImgYUV ,ImgDstTheta;

//IPFlowPoint tbl[32*22+1];
IPFlowPoint tbl[16*11+1];
IPFlowSize fsize;
20080311   */
unsigned char imgbuff[X_SIZE * Y_SIZE];

//  Omni CAM 人検出結果for sonsor fusion　　　　　 2004.9.18.
//内部処理用と受け渡し用変数
int omni_man_way, cam_man_dist;
//距離　　単位mm　　０〜５０００
int omni_man_direct, cam_man_dir;
//方角　　°　０〜３６０
int omni_accord, cam_man_accord;
//一致度
int omni_time, cam_man_time;
//検出時の時間
