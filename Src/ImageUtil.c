#include "ImageUtil.h"
#include "ImageCom.h"

void IplToArray(const IplImage* image_in,
                unsigned char image_out[3][Y_SIZE][X_SIZE]) {
  const int offset_x = 50;
  const unsigned char* data;
  int width, height;
  int x_scale, y_scale;
  int x, y;

  data = (const unsigned char*)image_in->imageData;
  width = image->width;
  height = image->height;

  if (height == 240) {
    y_scale = x_scale = 1;
  } else {
    y_scale = x_scale = 2;
  }

  for (y = 0; y < height; y += y_scale) {
    for (x = 0; x < width; x += x_scale) {
      int base = ((width - x) + y * width) * 3;
      int gx = x / x_scale;
      int gy = y / y_scale;

      if (offset_x < gx && gx < X_SIZE + offset_x) {
        image_out[IMAGE_R][gy][gx - offset_x] = data[base + 2];
        image_out[IMAGE_G][gy][gx - offset_x] = data[base + 1];
        image_out[IMAGE_B][gy][gx - offset_x] = data[base + 0];
      }
    }
  }
}

void toGrayScale(unsigned char image_out[3][Y_SIZE][X_SIZE]) {
  copy_image(image_out[IMAGE_G], image_out[IMAGE_R]);
  copy_image(image_out[IMAGE_G], image_out[IMAGE_B]);
}

void toGrayScaleOld(unsigned char image_out[3][Y_SIZE][X_SIZE]){
  copy_image(image_out[0], image_out[1]);
  copy_image(image_out[0], image_out[2]);
}
