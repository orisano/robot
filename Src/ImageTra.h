#ifndef __INCLUDE_IMAGE_TRA_H__
#define __INCLUDE_IMAGE_TRA_H__

int rgb_read(unsigned char *image, int xsize, int ysize, char *filename);
int rgb_write(unsigned char *image, int xsize, int ysize, char *filename);
int bmp_read(unsigned char *image, int xsize, int ysize, char *filename);
int bmp_read2(unsigned char *image, int xsize, int ysize, char *filename);
int bmp_write(unsigned char *image, int xsize, int ysize, char *filename);

#endif
