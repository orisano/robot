#ifndef __INCLUDE_LAPLACIA_H__
#define __INCLUDE_LAPLACIA_H__

#include "Params.h"

void laplacian(unsigned char image_in[Y_SIZE][X_SIZE],
               unsigned char image_out[Y_SIZE][X_SIZE], float amp);

void filter1(unsigned char img_in[Y_SIZE][X_SIZE],
             unsigned char img_out[Y_SIZE][X_SIZE], float amp);

#endif
