#ifndef __INCLUDE_TRAN_YSH_H__
#define __INCLUDE_TRAN_YSH_H__

#include "Params.h"

void tran_ysh(int in_y[Y_SIZE][X_SIZE], int in_sat[Y_SIZE][X_SIZE],
              int in_hue[Y_SIZE][X_SIZE], int out_y[Y_SIZE][X_SIZE],
              int out_sat[Y_SIZE][X_SIZE], int out_hue[Y_SIZE][X_SIZE],
              float ym, float sm, float hd);

#endif
