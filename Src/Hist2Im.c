#include "Hist2Im.h"

/*--- hist2_image --- ２次元ヒストグラムを求め画像化する ----------------------
        image_in1:	画像データ　Ｘ軸用
        image_in2:	画像データ　Ｙ軸用
        image_hist:	２次元ヒストグラム
-----------------------------------------------------------------------------*/
void hist2_image(unsigned char image_in1[Y_SIZE][X_SIZE],
                 unsigned char image_in2[Y_SIZE][X_SIZE],
                 unsigned char image_hist[Y_SIZE][X_SIZE]) {
  int i, j;

  for (i = 0; i < Y_SIZE; i++)
    for (j = 0; j < X_SIZE; j++) image_hist[i][j] = 0;
  for (i = 0; i < Y_SIZE; i++) image_hist[i][0] = HIGH;          /* Ｘ軸 */
  for (j = 0; j < X_SIZE; j++) image_hist[Y_SIZE - 1][j] = HIGH; /* Ｙ軸 */
}
