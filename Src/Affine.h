#ifndef __INCLUDE_AFFINE_H__
#define __INCLUDE_AFFINE_H__

#include "Params.h"

void affine(unsigned char img_in[Y_SIZE][X_SIZE],
            unsigned char img_out[Y_SIZE][X_SIZE], float deg, float zx,
            float zy, float px, float py);
void panorama(unsigned char img_in[Y_SIZE][X_SIZE],
               unsigned char img_out[Y_SIZE][X_SIZE], int center_x,
               int center_y);
void panorama2(unsigned char img_in[Y_SIZE][X_SIZE],
               unsigned char img_out[Y_SIZE][X_SIZE], int center_x,
               int center_y);
void panorama3(unsigned char img_in[Y_SIZE][X_SIZE],
               unsigned char img_out[Y_SIZE][X_SIZE], int center_x,
               int center_y);

#endif
