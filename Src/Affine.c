#include "Affine.h"
#include <math.h>

/*--- affine ---
拡大縮小,回転,移動（線形補間法）------------------------------
        img_in:	入力画像配列
        img_out:	出力画像配列
        deg:		回転角（度）
        zx:			拡大率（横）
        zy:			拡大率（縦）
        px:			移動量（横）
        py:			移動量（縦）
-----------------------------------------------------------------------------*/
void affine(unsigned char img_in[Y_SIZE][X_SIZE],
            unsigned char img_out[Y_SIZE][X_SIZE], float deg, float zx,
            float zy, float px, float py) {
  int i, j, m, n;
  float x, y, u, v, p, q;
  double r;
  float c, s;
  int xs = X_SIZE / 2;
  int ys = Y_SIZE / 2;
  int d;

  r = deg * M_PI / 180.0;
  c = (float)cos(r);
  s = (float)sin(r);
  for (i = -ys; i < ys; i++) {
    for (j = -xs; j < xs; j++) {
      v = i - py;
      u = j - px;
      y = (u * s + v * c) / zy;
      x = (u * c - v * s) / zx;
      if (y > 0)
        m = (int)y;
      else
        m = (int)(y - 1);
      if (x > 0)
        n = (int)x;
      else
        n = (int)(x - 1);
      q = y - m;
      p = x - n;
      if ((m >= -ys) && (m < ys) && (n >= -xs) && (n < xs))
        d = (int)((1.0 - q) * ((1.0 - p) * img_in[m + ys][n + xs] +
                               p * img_in[m + ys][n + 1 + xs]) +
                  q * ((1.0 - p) * img_in[m + 1 + ys][n + xs] +
                       p * img_in[m + 1 + ys][n + 1 + xs]));
      else
        d = 0;
      if (d < 0) d = 0;
      if (d > 255) d = 255;
      img_out[i + ys][j + xs] = d;
    }
  }
}

/*--- panorama ---
パノラマ展開　------------------------------------------
        img_in:	入力画像配列
        img_out:	出力画像配列
        center_x:	全方位画像中心ｘ座標
        center_y:	全方位画像中心ｙ座標
-----------------------------------------------------------------------------*/
void panorama(unsigned char img_in[Y_SIZE][X_SIZE],
              unsigned char img_out[Y_SIZE][X_SIZE], int center_x,
              int center_y) {
  int i, j, m, n;
  double r;
  int xs = 256;
  int ys = 105;

  r = M_PI / (double)(xs / 2);
  for (i = 0; i < xs; i++) {
    for (j = 0; j < ys; j++) {
      m = center_x + (int)((ys - j) * (float)cos(r * i));
      n = center_y + (int)((ys - j) * (float)sin(r * i));
      img_out[j][i] = img_in[n][m];
    }
  }
}
/*--- panorama ---
パノラマ展開2　------------------------------------------
        img_in:	入力画像配列
        img_out:	出力画像配列
        center_x:	全方位画像中心ｘ座標
        center_y:	全方位画像中心ｙ座標
-----------------------------------------------------------------------------*/
void panorama2(unsigned char img_in[Y_SIZE][X_SIZE],
               unsigned char img_out[Y_SIZE][X_SIZE], int center_x,
               int center_y) {
  int i, j, m, n;
  double r;
  int xs = 180;
  int ys = 125;
  r = M_PI / (double)(xs);
  for (i = 0; i < xs; i++) {
    for (j = 0; j < ys; j++) {
      m = center_x + (int)((j) * (float)cos(r * i));
      n = center_y + (int)((j) * (float)sin(r * i));
      img_out[j][i] = img_in[n][m];
    }
  }
  for (i = xs; i < 2 * xs; i++) {
    for (j = 0; j < ys; j++) {
      m = center_x + (int)((j) * (float)cos(r * i));
      n = center_y + (int)((j) * (float)sin(r * i));
      img_out[j + 128][i - 180] = img_in[n][m];
    }
  }
}
/*--- panorama3 ---
パノラマ展開3　------------------------------------------
 立山マシンカメラ画像パノラマ展開	2005.7.15.
        img_in:	入力画像配列
        img_out:	出力画像配列
        center_x:	全方位画像中心ｘ座標
        center_y:	全方位画像中心ｙ座標
-----------------------------------------------------------------------------*/
void panorama3(unsigned char img_in[Y_SIZE][X_SIZE],
               unsigned char img_out[Y_SIZE][X_SIZE], int center_x,
               int center_y) {
  int i, j, m, n;
  double r;
  int xs = 180;
  int ys = 125;
  int yp = 30;
  r = M_PI / (double)(xs / 2);
  for (i = 0; i < xs; i++) {
    for (j = yp; j < ys; j++) {
      m = center_x + (int)((j) * (float)cos(r * i));
      n = center_y + (int)((j) * (float)sin(r * i));
      img_out[j - yp][i] = img_in[n][m];
    }
  }
}
