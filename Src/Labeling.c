#include "Labeling.h"
#include "Params.h"
#include <string.h>

int uf_init(int *uf, int size);
int uf_merge(int *uf, int x, int y);
int uf_root(int *uf, int x);
int uf_size(int *uf, int x);

static const int dx[] = {1, 1, 1, 0, -1, -1, -1, 0};
static const int dy[] = {-1, 0, 1, 1, 1, 0, -1, -1};

/**
 * 2値化処理が終わった画像データに対してラベリング処理を行い最大面積のラベルのみを残す関数
 * ラベリング処理は素集合データ構造であるUnion-Findを用いている。
 * 参考URL:
 *http://ja.wikipedia.org/wiki/%E7%B4%A0%E9%9B%86%E5%90%88%E3%83%87%E3%83%BC%E3%82%BF%E6%A7%8B%E9%80%A0
 *
 * @param image_in 2値化処理済みの画像データ
 * @param image_out 最大面積のラベルのみが出力される画像データ
 * @param v ラベリングを行う画素の値
 * @param dest 最大面積のラベルを出力する際の値
 * @param undest 最大面積のラベル以外の領域を出力する際の値
 * @return 常に0
 *
 */
int labelingx(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_label[Y_SIZE][X_SIZE], int v, int dest,
              int undest) {
  int union_find[Y_SIZE * X_SIZE];
  int maxi;
  int x, y;
  int i;

  uf_init(union_find, sizeof(union_find));
  for (y = 0; y < Y_SIZE; ++y) {
    for (x = 0; x < X_SIZE; ++x) {
      /* 注目するpixelがラベリングを行う値でなければ次のpixelへ */
      if (image_in[y][x] != v) {
        continue;
      }
      /* 注目するpixelの8近傍を隣接領域として結合する */
      for (i = 0; i < 8; ++i) {
        int ny, nx;
        ny = y + dy[i];
        nx = x + dx[i];
        /* 近傍pixelが領域外であれば次の近傍pixelへ */
        if (ny < 0 || ny >= Y_SIZE || nx < 0 || nx >= X_SIZE) {
          continue;
        }
        if (image_in[y][x] == image_in[ny][nx]) {
          uf_merge(union_find, y * X_SIZE + x, ny * X_SIZE + nx);
        }
      }
    }
  }

  maxi = uf_root(union_find, 0);
  for (i = 1; i < Y_SIZE * X_SIZE; i++) {
    if (uf_size(union_find, i) > uf_size(union_find, maxi)) {
      maxi = uf_root(union_find, i);
    }
  }
  maxi = uf_root(union_find, maxi);

  for (y = 0; y < Y_SIZE; ++y) {
    for (x = 0; x < X_SIZE; ++x) {
      int id = y * X_SIZE + x;
      int r = uf_root(union_find, id);
      if (r != maxi) {
        image_label[y][x] = dest;
      } else {
        image_label[y][x] = undest;
      }
    }
  }
  return (0);
}

/**
 * Union-Findを初期化する関数
 * Union-Findは実装上の工夫から-1で初期化を行う。
 *
 * @param uf 初期化するUnion-Find管理領域
 * @param size 管理領域のbyte数
 * @return 常に0
 *
 */

int uf_init(int *uf, int size) {
  /**
   * ufを-1で高速に初期化を行う
   * ufはsignedでなければいけない
   */
  memset(uf, -1, size);
  return (0);
}

/**
 * Union-Findで2つの集合の併合を行う関数
 *
 * @param uf Union-Findの管理領域
 * @param x 併合を行う集合
 * @param y 併合を行う集合
 * @return 併合に成功した場合0, 失敗した場合1
 *
 */
int uf_merge(int *uf, int x, int y) {
  x = uf_root(uf, x);
  y = uf_root(uf, y);
  if (x != y) {
    if (uf[y] < uf[x]) {
      int tmp = x;
      x = y;
      y = tmp;
    }
    uf[x] += uf[y];
    uf[y] = x;
  }
  return (x == y);
}

/**
 * Union-Findで親に当たる要素を取得する関数
 * 基本的にUnion-Find内部で用いる関数
 *
 * @param uf Union-Findの管理領域
 * @param x 親を取得する要素
 * @return 親に当たる要素
 *
 */
int uf_root(int *uf, int x) {
  if (uf[x] < 0) {
    return (x);
  }
  uf[x] = uf_root(uf, uf[x]);
  return uf[x];
}

/**
 * Union-Findである要素を含む集合の要素の数を返す関数
 *
 * @param uf Union-Findの管理領域
 * @param x 要素の数を取得する集合に含まれている要素
 * @return 要素の数
 *
 */
int uf_size(int *uf, int x) { return (-uf[uf_root(uf, x)]); }
