#ifndef __INCLUDE_PERSPECT_H__
#define __INCLUDE_PERSPECT_H__

#include "Params.h"

void perspect(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_out[Y_SIZE][X_SIZE], float ax, float ay,
              float px, float py, float pz, float rz, float rx, float ry,
              float v, float s);

#endif
