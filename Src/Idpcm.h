#ifndef __INCLUDE_IDPCM_H__
#define __INCLUDE_IDPCM_H__

#include "Params.h"

void idpcm1(short data_in[X_SIZE], int line,
            unsigned char image_out[Y_SIZE][X_SIZE]);

void idpcm2(short data_in[X_SIZE], int line,
            unsigned char image_out[Y_SIZE][X_SIZE]);

#endif
