#ifndef __INCLUDE_THINING_H__
#define __INCLUDE_THINING_H__

#include "Params.h"

void thinning(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_out[Y_SIZE][X_SIZE]);

#endif
