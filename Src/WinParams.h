#ifndef _WIN_PARAM_H
#define _WIN_PARAM_H

#include "Params.h"
#include <stdio.h>
#include <stdlib.h>
#if OFF_DEMO
#include <windows.h>
#else if ON_DEMO
#include "stdafx.h"
#endif
#include "Params.h"

#define WIN 0
#define WOUT 1
#define WWORK 2
#define WKEY 3

#define MENU_POSX 10 /* メニューウィンドウの横方向表示位置（左端）	*/
#define MENU_POSY 0  /* メニューウィンドウの縦方向表示位置（上端）	*/
#define MENU_SIZEX \
  600 /* メニューウィンドウの横方向表示サイズ			*/
#define MENU_SIZEY \
  105  // 40	/* メニューウィンドウの縦方向表示サイズ			*/

#define WIN_POSX 10  /* 入力画像ウィンドウの横方向表示位置（左端）	*/
#define WIN_POSY 105 /* 入力画像ウィンドウの縦方向表示位置（上端）	*/

#define WOUT_POSX 10 /* 出力画像ウィンドウの横方向表示位置（左端）	*/
#define WOUT_POSY                                                             \
  390 /* 出力画像ウィンドウの縦方向表示位置（上端） 320 access violation なし \
         */

#define WWORK_POSX 274 /* ワーク画像ウィンドウの横方向表示位置（左端）	*/
#define WWORK_POSY 105 /* ワーク画像ウィンドウの縦方向表示位置（上端）	*/

#define WKEY_POSX 274 /* キー画像ウィンドウの横方向表示位置（左端）	*/
#define WKEY_POSY                                                             \
  390 /* キー画像ウィンドウの縦方向表示位置（上端） 320 access violation なし \
         */

#define MAX_NAME_LENGTH 256

#define P_X_SIZE X_SIZE * 4
#define P_Y_SIZE Y_SIZE * 4

typedef struct {
  HWND hWnd;
  HBITMAP hBitmap;
  //	HANDLE	hR, hG, hB;
} ImageWin;

#endif