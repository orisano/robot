#include "Thresh.h"

/*--- threshold --- 閾（しきい）値処理 ----------------------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        thresh:		閾値
        mode:		閾値処理の方法(1,2)
-----------------------------------------------------------------------------*/
void threshold(unsigned char image_in[Y_SIZE][X_SIZE],
               unsigned char image_out[Y_SIZE][X_SIZE], int thresh, int mode) {
  int i, j;

  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      switch (mode) {
        case 2:
          if ((int)image_in[i][j] <= thresh)
            image_out[i][j] = HIGH;
          else
            image_out[i][j] = LOW;
          break;
        default:
          if ((int)image_in[i][j] >= thresh)
            image_out[i][j] = HIGH;
          else
            image_out[i][j] = LOW;
          break;
      }
    }
  }
}
/*--- threshold --- 閾（しきい）値処理 ----------------------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        thresh1		下側閾値
        thresh2:	上側閾値
-----------------------------------------------------------------------------*/
void thresh_gray(unsigned char image_in[Y_SIZE][X_SIZE],
                 unsigned char image_out[Y_SIZE][X_SIZE], int thresh1,
                 int thresh2, int mode) {
  int i, j;

  switch (mode) {
    case 0:
      for (i = 0; i < Y_SIZE; i++) {
        for (j = 0; j < X_SIZE; j++) {
          if (thresh1 >= (int)image_in[i][j]) {
            image_out[i][j] = 255;
          } else {
            image_out[i][j] = image_in[i][j];  // 0;
          }
        }
      }
      break;
    case 1:
      for (i = 0; i < Y_SIZE; i++) {
        for (j = 0; j < X_SIZE; j++) {
          if ((int)image_in[i][j] >= thresh2) {
            image_out[i][j] = 255;
          } else {
            image_out[i][j] = image_in[i][j];  // 0;
          }
        }
      }
      break;
    case 2:
      for (i = 0; i < Y_SIZE; i++) {
        for (j = 0; j < X_SIZE; j++) {

          if ((int)image_in[i][j] <= thresh2 &&
              (int)image_in[i][j] >= thresh1) {
            // image_out[i][j] = 0;//255;
            image_out[i][j] = 255;  // image_in[i][j];
          } else {
            image_out[i][j] = 0;
          }
        }
      }
      break;
    case 3:
      for (i = 0; i < Y_SIZE; i++) {
        for (j = 0; j < X_SIZE; j++) {
          if (image_in[i][j] <= thresh1) {
            image_out[i][j] = 0;
          } else {
            image_out[i][j] = 255;
          }
        }
      }
      break;

    case 4:
      for (i = 0; i < Y_SIZE; i++) {
        for (j = 0; j < X_SIZE; j++) {
          if ((int)image_in[i][j] <= thresh2 &&
              (int)image_in[i][j] >= thresh1) {
            // image_out[i][j] = 0;//255;
            image_out[i][j] = 0;  // image_in[i][j];
          } else {
            image_out[i][j] = 255;
          }
        }
      }
      break;
  }
}
/*--- thresh_3d --- ３D画像向け　閾（しきい）値処理 ----------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        thresh1		下側閾値
        thresh2:	上側閾値
-----------------------------------------------------------------------------*/
void thresh_3d(unsigned char image_in[Y_SIZE][X_SIZE],
               unsigned char image_out[Y_SIZE][X_SIZE], int thresh1,
               int thresh2) {
  int i, j;
  int range;
  double scale;
  int tmp;
  int min, max;

  min = 255;
  max = 0;
  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      if ((int)image_in[i][j] <= thresh2 && (int)image_in[i][j] >= thresh1) {
        if (max < image_in[i][j]) max = image_in[i][j];
        if (min > image_in[i][j]) min = image_in[i][j];
      }
    }
  }

  range = max - min;
  scale = (double)256 / range;

  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      if ((int)image_in[i][j] <= thresh2 && (int)image_in[i][j] >= thresh1) {
        tmp = (int)((image_in[i][j] - min) * scale + 0.5);
        if (tmp < 0)
          tmp = 0;
        else if (tmp > 255)
          tmp = 255;
        image_out[i][j] = (unsigned char)tmp;
      } else
        image_out[i][j] = LOW;
    }
  }
}

/*--- thresh_3d_1 --- ３D画像向け　閾（しきい）値処理
----------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        thresh1		下側閾値
        thresh2:	上側閾値
-----------------------------------------------------------------------------*/
void thresh_3d_1(unsigned char image_in[Y_SIZE][X_SIZE],
                 unsigned char image_out[Y_SIZE][X_SIZE], int thresh1,
                 int thresh2) {
  int i, j;
  int range;
  double scale;
  int tmp;
  int min, max;

  scale = 2.0;
  min = 0;
  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      if ((int)image_in[i][j] <= thresh2 && (int)image_in[i][j] >= thresh1) {
        tmp = (int)((image_in[i][j] - min) * scale + 0.5);
        if (tmp < 0)
          tmp = 0;
        else if (tmp > 255)
          tmp = 255;
        image_out[i][j] = (unsigned char)tmp;
      } else
        image_out[i][j] = LOW;
    }
  }
}

/*--- thresh_full_view--- 天井全景マッチング向け　閾値処理 ---------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        thresh1		閾値
        thresh2:	代入値
-----------------------------------------------------------------------------*/
void thresh_full_view(unsigned char img_in[Y_SIZE][X_SIZE],
                      unsigned char img_out[Y_SIZE][X_SIZE], int thresh1,
                      int thresh2) {
  int i, j;

  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      if ((int)img_in[i][j] <= 50) {
        img_out[i][j] = 0;
      } else {
        img_out[i][j] = (int)img_in[i][j];
      }
    }
  }
}
