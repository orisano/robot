#ifndef __INCLUDE_FFT2_H__
#define __INCLUDE_FFT2_H__

#include "Params.h"

int ff2(float a_rl[Y_SIZE][X_SIZE], float a_im[Y_SIZE][X_SIZE], int inv);

#endif
