//=============================================================================
// System Includes
//=============================================================================
#pragma warning(disable : 4995)
#define _CRT_SECURE_NO_WARNINGS
#include <assert.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <string.h>
#include <stdlib.h>
#include <highgui.h>

#include "WinParams.h"
#include "WinCImage.h"
#include "Proto.h"
#include "WinProto.h"
#include "cv_drone.h"

HDC hMemDC;
PAINTSTRUCT ps;
int bar_one, bar_two;
BOOL g_bRun = FALSE;  // 2009.01.08.Y
POINT point;
HANDLE hThread;
DWORD dThreadID;

LRESULT CALLBACK MainWndProc(HWND, UINT, WPARAM, LPARAM);
BOOL InitApplication(HANDLE);
BOOL InitInstance(HANDLE, int);
void StartThread();
void EndThread();
void AREA_DETECT_Thread_White();

/*--- WinMain --- メイン ------------------------------------------------------
        hInstance:		アプリケーションへのハンドル
        hPrevInstance:	直前のインスタンスハンドル
        lpCmdLine:		コマンドラインの文字列
        nCmdShow:		起動時の表示方法
-----------------------------------------------------------------------------*/
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine, int nCmdShow) {
  MSG msg;

  if (!hPrevInstance)
    if (!InitApplication(hInstance)) return 0;
  if (!InitInstance(hInstance, nCmdShow)) return 0;
  ghInst = hInstance;
  while (GetMessage(&msg, (HWND)NULL, (UINT)NULL, (UINT)NULL)) {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
  return msg.wParam;
}

/*--- MainWndProc --- メッセージの処理 ----------------------------------------
        hWnd:		ウィンドウ質問
        message:	メッセージ
        wParam:		ワードパラメータ
        lParam:		ロングパラメータ
-----------------------------------------------------------------------------*/
LRESULT CALLBACK
MainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
  HDC hDC;
  int i, j;
  int wno;
  int limit_input;
  int thres_min, thres_max;
  static HWND scroll_1, label_1;
  static HWND scroll_2, label_2;
  static SCROLLINFO scr, scr_;
  static TCHAR strScroll[32];
  HWND scroller = 0;
  HWND labeler = 0;
  SCROLLINFO *info;
  int wait_ms;
  int mode = 3;
  float ym, sm, hd;
  float a, b, c, x0, yy, deg;
  float amp;
  char filename[128];

  switch (message) {
    case WM_RBUTTONDOWN:
      if (hWnd != imageWin[WIN].hWnd && hWnd != imageWin[WOUT].hWnd &&
          hWnd != imageWin[WWORK].hWnd && hWnd != imageWin[WKEY].hWnd)
        break;
      point.x = LOWORD(lParam);
      point.y = HIWORD(lParam);
      ClientToScreen(hWnd, &point);
      TrackPopupMenu(hSubMenu, 0, point.x, point.y, 0, hWnd, NULL);
      break;
    case WM_COMMAND:
      switch (wParam) {
        case MENU_READ:
          if (!GetOpenFileName((LPOPENFILENAME) & ofn)) return FALSE;
          if (bmp_read((unsigned char*)image_in[0], X_SIZE, Y_SIZE, ofn.lpstrFileTitle) != -1)
            DisplayCImage(image_in, WIN);
          else
            MessageBox(GetFocus(), "File open error", "Error",
                       MB_OK | MB_APPLMODAL);
          break;
        case MENU_SEQ_READ:
          if (!GetParams1("繰り返し画像ファイル入力回数", &a)) break;
          limit_input = (int)a;
          for (i = 0; i < limit_input; i++) {

            sprintf(filename, "%s%.4d", "cam", i);
            if (bmp_read((unsigned char*)image_in[0], X_SIZE, Y_SIZE, filename) == -1) {
            }
            toGrayScaleOld(image_in);
            DisplayCImageFast(image_in, WIN);
            Wait(10);
          }
          break;
        case MENU_WRITE:
          if (!GetSaveFileName((LPOPENFILENAME) & ofn)) return FALSE;
          if (bmp_write((unsigned char*)image_out[0], X_SIZE, Y_SIZE, ofn.lpstrFileTitle) == -1)
            MessageBox(GetFocus(), "File open error", "Error",
                       MB_OK | MB_APPLMODAL);
          break;
        case MENU_CAMERA_IN:
          init_cam_usb_std();
          grab_cam_usb_std(image_in);
          toGrayScale(image_in);
          DisplayCImage(image_in, WIN);
          close_cam_usb_std();
          break;

        // INにカメラ連続入力
        case MENU_SEQ_CAMERA_IN:
          init_cam_usb_std();
          grab_cam_usb_std(image_in);
          toGrayScale(image_in);
          DisplayCImage(image_in, WWORK);
          if (!GetParams2("カメラ入力指定", "繰返し回数", "入力時間間隔(ms)", &a,
                          &b)) {
            break;
          }
          limit_input = (int)a;
          wait_ms = (int)b;
          for (i = 0; i < limit_input; i++) {
            grab_cam_usb_std(image_in);
            toGrayScale(image_in);
            DisplayCImageFast(image_in, WIN);
            Wait(wait_ms);
          }
          close_cam_usb_std();
          break;

        case MENU_SEQ_CAMERA_SAVE:
          init_cam_usb_std();
          grab_cam_usb_std(image_in);
          toGrayScale(image_in);
          DisplayCImage(image_in, WKEY);
          if (!GetParams2("カメラ入力指定(画像保存)", "繰返し回数",
                          "入力時間間隔(ms)", &a, &b))
            break;
          limit_input = (int)a;
          wait_ms = (int)b;
          for (i = 0; i < limit_input; i++) {
            grab_cam_usb_std(image_in);
            toGrayScale(image_in);
            DisplayCImageFast(image_in, WOUT);
            sprintf(filename, "%d", i);
            if (bmp_write((unsigned char*)image_in[0], X_SIZE, Y_SIZE, filename) == -1)
              MessageBox(GetFocus(), "File open error", "Error",
                         MB_OK | MB_APPLMODAL);
            Wait(wait_ms);
          }
          close_cam_usb_std();

          break;

        //領域検出
        case AREA_DETECT:
          if (g_bRun == FALSE)
            StartThread();
          else
            EndThread();
          break;

        //連続閾値処理
        case MENU_SEQ_THRESHOLD:
          init_cam_usb_std();
          if (!GetParams3("閾値", "下限", "上限", "モード", &a, &b, &c)) {
            close_cam_usb_std();
            break;
          }

          while (1) {
            int mode_ext = (int)c;
            if (mode_ext >= 0) {
              grab_cam_usb_std(image_in);
              toGrayScale(image_in);
              DisplayCImage(image_in, WIN);
            }
            thres_min = (int)a;
            thres_max = (int)b;
            mode = mode_ext >= 0 ? mode_ext : -mode_ext;
            thresh_gray(image_in[0], image_out[0], thres_min, thres_max, mode);
            toGrayScaleOld(image_out);
            DisplayCImage(image_out, WOUT);
            if (!GetParams2("sugoi", "UPPER", "LOWER", &b, &a)) break;
            DisplayCImage(image_out, WOUT);
          }
          close_cam_usb_std();
          break;

        //追加-------------20130615--ArDrone----------------
        case MENU_ARDRONE:
          cv_drone();
          break;
        case MENU_ARDRONE_TAKEOFF:
          cv_drone_takeoff();
          break;
        case MENU_ARDRONE_LANDING:
          cv_drone_landing();
          break;
        case MENU_ARDRONE_SETCAMERA:  // 20130701
          cv_drone_setCamera();
          break;
        case MENU_ARDRONE_CAM_IN:  // 20130702-0704
          cv_drone_cam_in(image_in);
          toGrayScale(image_in);
          DisplayCImage(image_in, WIN);
          break;
        case MENU_ARDRONE_INIT:  // 20130703
          cv_drone_init();
          break;
        case MENU_ARDRONE_CLOSE:  // 20130703
          cv_drone_close();
          break;
        //追加-------------20130702--ArDrone----------------

        case MENU_THRESHOLD:
          if (!GetOpenFileName((LPOPENFILENAME) & ofn)) return FALSE;
          if (bmp_read((unsigned char*)image_in[0], X_SIZE, Y_SIZE, ofn.lpstrFileTitle) != -1)
            DisplayCImage(image_in, WIN);
          else
            MessageBox(GetFocus(), "File open error", "Error",
                       MB_OK | MB_APPLMODAL);
          for (i = 0; i < 10; i++) {
            if (!GetParams3("閾値", "下限", "上限", "モード", &a, &b, &c))
              break;
            thres_min = (int)a;
            thres_max = (int)b;
            mode = (int)c;
            thresh_gray(image_in[0], image_out[0], thres_min, thres_max, mode);
            toGrayScaleOld(image_in);
            DisplayCImage(image_out, WOUT);
            if (!GetParams1("OK(10)/NG(0)", &amp)) break;
            if ((int)amp > 10) break;
          }
          DisplayCImage(image_out, WOUT);
          break;

        case Thread:
          if (!GetParams1("start end", &a)) break;
          if ((int)a == 1) {
            StartThread();
          } else {
            EndThread();
          }
          break;
        case MENU_3D_INPUT:
          break;
        case MENU_3D_DIST_INPUT:
          break;
        case MENU_QUIT:
          DestroyWindow(hWnd);
          break;
        case MENU_CLEAR:
          if (hWnd == imageWin[WIN].hWnd) {
            image_clear_color(image_in);
            DisplayCImage(image_in, WIN);
          } else if (hWnd == imageWin[WOUT].hWnd) {
            image_clear_color(image_out);
            DisplayCImage(image_out, WOUT);
          } else if (hWnd == imageWin[WWORK].hWnd) {
            image_clear_color(image_work);
            DisplayCImage(image_work, WWORK);
          } else if (hWnd == imageWin[WKEY].hWnd) {
            image_clear_color(image_key);
            DisplayCImage(image_key, WKEY);
          }
          break;
        case MENU_CLEAR_IN:
          image_clear_color(image_in);
          DisplayCImage(image_in, WIN);
          break;
        case MENU_CLEAR_OUT:
          image_clear_color(image_out);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_CLEAR_WORK:
          image_clear_color(image_work);
          DisplayCImage(image_work, WWORK);
          break;
        case MENU_CLEAR_KEY:
          image_clear_color(image_key);
          DisplayCImage(image_key, WKEY);
          break;
        case MENU_COPY:
          hDC = GetDC(hWnd);
          hBitmap = CreateCompatibleBitmap(hDC, X_SIZE, Y_SIZE);
          if (hWnd == imageWin[WIN].hWnd)
            RgbToBitmap(image_in[0], image_in[1], image_in[2], hBitmap);
          else if (hWnd == imageWin[WOUT].hWnd)
            RgbToBitmap(image_out[0], image_out[1], image_out[2], hBitmap);
          else if (hWnd == imageWin[WWORK].hWnd)
            RgbToBitmap(image_work[0], image_work[1], image_work[2], hBitmap);
          else if (hWnd == imageWin[WKEY].hWnd)
            RgbToBitmap(image_key[0], image_key[1], image_key[2], hBitmap);
          else
            break;
          OpenClipboard(NULL);
          EmptyClipboard();
          SetClipboardData(CF_BITMAP, hBitmap);
          CloseClipboard();
          break;
        case MENU_PASTE:
          OpenClipboard(NULL);
          hBitmap = (HBITMAP)GetClipboardData(CF_BITMAP);
          BitmapToRgb(hBitmap, image_buf[0], image_buf[1], image_buf[2]);
          CloseClipboard();
          if (hWnd == imageWin[WIN].hWnd) {
            image_copy_color(image_buf, image_in);
            DisplayCImage(image_in, WIN);
          } else if (hWnd == imageWin[WOUT].hWnd) {
            image_copy_color(image_buf, image_out);
            DisplayCImage(image_out, WOUT);
          } else if (hWnd == imageWin[WWORK].hWnd) {
            image_copy_color(image_buf, image_work);
            DisplayCImage(image_work, WWORK);
          } else if (hWnd == imageWin[WKEY].hWnd) {
            image_copy_color(image_buf, image_key);
            DisplayCImage(image_key, WKEY);
          }
          break;
        case MENU_COPY_CI:
          OpenClipboard(NULL);
          hBitmap = (HBITMAP)GetClipboardData(CF_BITMAP);
          BitmapToRgb(hBitmap, image_buf[0], image_buf[1], image_buf[2]);
          CloseClipboard();
          image_copy_color(image_buf, image_in);
          DisplayCImage(image_in, WIN);
          break;
        case MENU_COPY_OC:
          hDC = GetDC(hWnd);
          hBitmap = CreateCompatibleBitmap(hDC, X_SIZE, Y_SIZE);
          RgbToBitmap(image_out[0], image_out[1], image_out[2], hBitmap);
          OpenClipboard(NULL);
          EmptyClipboard();
          SetClipboardData(CF_BITMAP, hBitmap);
          CloseClipboard();
          break;
        case MENU_COPY_IO:
          image_copy_color(image_in, image_out);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_COPY_IW:
          image_copy_color(image_in, image_work);
          DisplayCImage(image_work, WWORK);
          break;
        case MENU_COPY_IK:
          image_copy_color(image_in, image_key);
          DisplayCImage(image_key, WKEY);
          break;
        case MENU_COPY_OI:
          image_copy_color(image_out, image_in);
          DisplayCImage(image_in, WIN);
          break;
        case MENU_COPY_OW:
          image_copy_color(image_out, image_work);
          DisplayCImage(image_work, WWORK);
          break;
        case MENU_COPY_OK:
          image_copy_color(image_out, image_key);
          DisplayCImage(image_key, WKEY);
          break;
        case MENU_COPY_WI:
          image_copy_color(image_work, image_in);
          DisplayCImage(image_in, WIN);
          break;
        case MENU_COPY_WO:
          image_copy_color(image_work, image_out);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_COPY_WK:
          image_copy_color(image_work, image_key);
          DisplayCImage(image_key, WKEY);
          break;
        case MENU_COPY_KI:
          image_copy_color(image_key, image_in);
          DisplayCImage(image_in, WIN);
          break;
        case MENU_COPY_KO:
          image_copy_color(image_key, image_out);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_COPY_KW:
          image_copy_color(image_key, image_work);
          DisplayCImage(image_work, WWORK);
          break;
        case MENU_COPY_IP_IN:
        case MENU_COPY_IP_OUT:
        case MENU_COPY_IP_WORK:
        case MENU_COPY_IP_KEY:
          toGrayScaleOld(image_in);
          DisplayCImage(image_in, WIN);
          break;
        case MENU_COPY_IN_IP:
        case MENU_COPY_OUT_IP:
        case MENU_COPY_WORK_IP:
        case MENU_COPY_KEY_IP:
          if (!GetOpenFileName((LPOPENFILENAME) & ofn)) return FALSE;
          if (bmp_read((unsigned char*)image_out[0], X_SIZE, Y_SIZE, ofn.lpstrFileTitle) != -1)
            DisplayCImage(image_out, WOUT);
          else
            MessageBox(GetFocus(), "File open error", "Error",
                       MB_OK | MB_APPLMODAL);
          DisplayCImage(image_out, WOUT);
          break;

        case MENU_HIST1:
          histgram(image_in[0], hist);
          histimage(hist, image_out[0]);
          image_copy(image_black, image_out[1]);
          image_copy(image_black, image_out[2]);
          DisplayCImage(image_out, WOUT);
          histgram(image_in[1], hist);
          histimage(hist, image_work[1]);
          image_copy(image_black, image_work[0]);
          image_copy(image_black, image_work[2]);
          DisplayCImage(image_work, WWORK);
          histgram(image_in[2], hist);
          histimage(hist, image_key[2]);
          image_copy(image_black, image_key[0]);
          image_copy(image_black, image_key[1]);
          DisplayCImage(image_key, WKEY);
          break;
        case MENU_HIST2:
          if (!GetSelection3("横軸", "Ｒ", "Ｇ", "Ｂ", &i)) break;
          if (!GetSelection3("縦軸", "Ｒ", "Ｇ", "Ｂ", &j)) break;
          hist2_image(image_in[i - 1], image_in[j - 1], image_work[0]);
          toGrayScaleOld(image_work);
          DisplayCImage(image_work, WWORK);
          break;

        case MENU_HARDKEY:
          if (!GetParams2("彩度-閾値", "最小", "最大", &a, &b)) break;
          thres_min = (int)a;
          thres_max = (int)b;
          thresh_gray(image_work[0], image_out[0], thres_min, thres_max, mode);
          toGrayScaleOld(image_out);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_SOFTKEY:
          if (!GetParams2("色相-閾値", "最小", "最大", &a, &b)) break;
          thres_min = (int)a;
          thres_max = (int)b;
          thresh_gray(image_key[0], image_out[0], thres_min, thres_max, mode);
          toGrayScaleOld(image_out);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_SYNTHESIS:
          synth(image_in[0], image_in[1], image_in[2], image_work[0],
                image_work[1], image_work[2], image_out[0], image_out[1],
                image_out[2], image_key[0]);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_S_SYNTHESIS:
          if (!GetOpenFileName((LPOPENFILENAME) & ofn)) return FALSE;
          if (bmp_read((unsigned char*)image_work[0], X_SIZE, Y_SIZE, ofn.lpstrFileTitle) != -1)
            DisplayCImage(image_work, WWORK);
          else
            MessageBox(GetFocus(), "File open error", "Error",
                       MB_OK | MB_APPLMODAL);
          toGrayScaleOld(image_work);
          DisplayCImage(image_work, WWORK);
          break;
        case MENU_COLORBAR:
          break;

        // 2004.6.17.色相　入り口
        case MENU_HSV_GEN: /* 色相　入り口　　　　2004.6.17. */
          if (!GetParams1("基準となる彩度値", &a)) break;
          rgb_to_yc(image_in[0], image_in[1], image_in[2], y, c1, c2);
          c_to_sh(c1, c2, sat, hue);
          for (i = 0; i < Y_SIZE; i++)
            for (j = 0; j < X_SIZE; j++) {
              if (y[i][j] < 0) y[i][j] = 0;
              if (y[i][j] > 255) y[i][j] = 255;
              image_out[0][i][j] = y[i][j];
            }
          sat_image(sat, image_work[0]);
          hue_image(sat, hue, a, image_key[0]);
          toGrayScaleOld(image_out);
          DisplayCImage(image_out, WOUT);
          toGrayScaleOld(image_work);
          DisplayCImage(image_work, WWORK);
          toGrayScaleOld(image_key);
          DisplayCImage(image_key, WKEY);
          break;
        case MENU_HSV_CHAN:
          if (!GetParams1("輝度の乗数", &ym)) break;
          if (!GetParams1("彩度の乗数", &sm)) break;
          if (!GetParams1("色相の増分", &hd)) break;
          rgb_to_yc(image_in[0], image_in[1], image_in[2], y, c1, c2);
          c_to_sh(c1, c2, sat, hue);
          tran_ysh(y, sat, hue, y, sat, hue, ym, sm, hd);
          sh_to_c(sat, hue, c1, c2);
          yc_to_rgb(y, c1, c2, image_out[0], image_out[1], image_out[2]);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_GRADIENT:
          if (!GetParams1("出力画像の利得", &amp)) break;
          for (i = 0; i < 3; i++) gradient(image_in[i], image_out[i], amp);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_LAPLACE:
          if (!GetParams1("出力画像の利得", &amp)) break;
          for (i = 0; i < 3; i++) laplacian(image_in[i], image_out[i], amp);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_TEMPLATE:
          if (!GetParams1("出力画像の利得", &amp)) break;
          for (i = 0; i < 3; i++) tmplate1(image_in[i], image_out[i], amp);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_SMOOTH:
          for (i = 0; i < 3; i++) smooth(image_in[i], image_out[i]);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_MEDIAN:
          for (i = 0; i < 3; i++) median(image_in[i], image_out[i]);
          DisplayCImage(image_out, WOUT);
          break;
        case MENU_SCALE_NE:
          break;
        case MENU_SCALE:
          break;
        case MENU_SHIFT:
          break;
        case MENU_ROTATE:
          break;
        case MENU_AFFINE:
          if (!GetParams2("拡大率", "横", "縦", &a, &b)) break;
          if (!GetParams2("移動量", "横", "縦", &x0, &yy)) break;
          if (!GetParams1("回転角（度）", &deg)) break;
          for (i = 0; i < 3; i++) {
            affine(image_in[i], image_out[i], deg, a, b, x0, yy);
            DisplayCImage(image_out, WOUT);
          }
          break;
      }
      break;
    case WM_PAINT:
      wno = -1;
      for (i = 0; i < 4; i++)
        if (hWnd == imageWin[i].hWnd) wno = i;
      if (wno == -1) return DefWindowProc(hWnd, message, wParam, lParam);
      BeginPaint(imageWin[wno].hWnd, &ps);
      hMemDC = CreateCompatibleDC(ps.hdc);
      SelectObject(hMemDC, imageWin[wno].hBitmap);
      BitBlt(ps.hdc, 0, 0, X_SIZE, Y_SIZE, hMemDC, 0, 0, SRCCOPY);
      DeleteDC(hMemDC);
      EndPaint(hWnd, &ps);
      break;
    case WM_DESTROY:
      PostQuitMessage(0);
      break;
    case WM_CREATE:
      if (scroll_1) break;
      scroll_1 = CreateWindow(
          TEXT("SCROLLBAR"), TEXT(""), WS_CHILD | WS_VISIBLE | SS_LEFT, 0, 0,
          200, 20, hWnd, (HMENU)1, ((LPCREATESTRUCT)(lParam))->hInstance, NULL);
      scroll_2 = CreateWindow(
          TEXT("SCROLLBAR"), TEXT(""), WS_CHILD | WS_VISIBLE | SS_LEFT, 0, 20,
          200, 20, hWnd, (HMENU)2, ((LPCREATESTRUCT)(lParam))->hInstance, NULL);
      label_1 = CreateWindow(
          TEXT("STATIC"), TEXT("0"), WS_CHILD | WS_VISIBLE | SS_LEFT, 220, 0,
          50, 20, hWnd, (HMENU)3, ((LPCREATESTRUCT)(lParam))->hInstance, NULL);
      label_2 = CreateWindow(
          TEXT("STATIC"), TEXT("0"), WS_CHILD | WS_VISIBLE | SS_LEFT, 220, 20,
          50, 20, hWnd, (HMENU)4, ((LPCREATESTRUCT)(lParam))->hInstance, NULL);
      scr.cbSize = sizeof(SCROLLINFO);
      scr.fMask = SIF_PAGE | SIF_RANGE;
      scr.nMin = 0;
      scr.nMax = 255;
      scr.nPage = 1;
      scr_ = scr;
      SetScrollInfo(scroll_1, SB_CTL, &scr, TRUE);
      SetScrollInfo(scroll_2, SB_CTL, &scr_, TRUE);
      scr_.fMask = scr.fMask = SIF_POS;
      break;
    case WM_HSCROLL:
      if ((HWND)lParam == scroll_1) {
        scroller = scroll_1;
        labeler = label_1;
        info = &scr;
      } else {
        scroller = scroll_2;
        labeler = label_2;
        info = &scr_;
      }
      switch (LOWORD(wParam)) {
        case SB_LEFT:
          info->nPos = info->nMin;
          break;
        case SB_RIGHT:
          info->nPos = info->nMax;
          break;
        case SB_LINELEFT:
          info->nPos--;
          break;
        case SB_LINERIGHT:
          info->nPos++;
          break;
        case SB_PAGELEFT:
          info->nPos -= info->nPage;
          break;
        case SB_PAGERIGHT:
          info->nPos += info->nPage;
          break;
        case SB_THUMBPOSITION:
          info->nPos = HIWORD(wParam);
          break;
        case SB_THUMBTRACK:
          info->nPos = HIWORD(wParam);
          break;
      }
      if (info->nPos < 0) info->nPos = 0;
      if (info->nPos > 255) info->nPos = 255;

      SetScrollInfo(scroller, SB_CTL, info, TRUE);
      wsprintf(strScroll, "%d", info->nPos);
      SetWindowText(labeler, strScroll);
      bar_one = scr.nPos;
      bar_two = scr_.nPos;
      break;
      
    default:
      return DefWindowProc(hWnd, message, wParam, lParam);
  }
  return 0;
}

/*--- InitApplication --- ウィンドウクラスの登録 ------------------------------
        hInstance:		アプリケーションへのハンドル
-----------------------------------------------------------------------------*/
BOOL InitApplication(HANDLE hInstance) {
  WNDCLASSEX wc, wcm;

  wc.cbSize = sizeof(wc);
  wc.style = CS_HREDRAW | CS_VREDRAW;
  wc.lpfnWndProc = (WNDPROC)MainWndProc;
  wc.cbClsExtra = 0;
  wc.cbWndExtra = 0;
  wc.hInstance = (HINSTANCE)hInstance;
  wc.hIcon = LoadIcon((HINSTANCE)hInstance, IDI_APPLICATION);
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  wc.lpszMenuName = "MainMenu";
  wc.lpszClassName = "MainClass";
  wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  if (!RegisterClassEx(&wc)) return FALSE;
  wcm.cbSize = sizeof(wcm);
  wcm.style = CS_HREDRAW | CS_VREDRAW;
  wcm.lpfnWndProc = (WNDPROC)MainWndProc;
  wcm.cbClsExtra = 0;
  wcm.cbWndExtra = 0;
  wcm.hInstance = (HINSTANCE)hInstance;
  wcm.hIcon = LoadIcon((HINSTANCE)hInstance, IDI_APPLICATION);
  wcm.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcm.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  wcm.lpszClassName = "ImageClass";
  wcm.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  return RegisterClassEx(&wcm);
}

/*--- InitInstance --- ウィンドウの作成 ---------------------------------------
        hInstance:		アプリケーションへのハンドル
        nCmdShow:		起動時の表示方法
-----------------------------------------------------------------------------*/
BOOL InitInstance(HANDLE hInstance, int nCmdShow) {
  HWND hWnd;
  HDC hDC;
  RECT rect;
  int i, j;

  hGWnd = hWnd =
      CreateWindow("MainClass", "RobotVisionシミュレータ 2013.6.15. 姉崎",
                   WS_OVERLAPPEDWINDOW, MENU_POSX, MENU_POSY, MENU_SIZEX,
                   MENU_SIZEY, 0, 0, (HINSTANCE)hInstance, 0);
  if (hWnd == NULL) return FALSE;
  ShowWindow(hWnd, nCmdShow);
  UpdateWindow(hWnd);
  hSubMenu = LoadMenu((HINSTANCE)ghInst, "SubMenu");
  hSubMenu = GetSubMenu(hSubMenu, 0);
  hDC = GetDC(hWnd);
  bitspixel = GetDeviceCaps(hDC, BITSPIXEL);
  if (bitspixel == 16) {
    MessageBox(GetFocus(),
               "フルカラー（２４ビットか３２ビット）をお勧めします。", "警告",
               MB_ICONHAND | MB_APPLMODAL);
  } else if (bitspixel != 24 && bitspixel != 32) {
    MessageBox(GetFocus(),
               "表示をフルカラー（２４ビットか３２ビット）にしてください。",
               "エラー", MB_ICONHAND | MB_APPLMODAL);
    return FALSE;
  }

  for (i = 0; i < 4; i++)
    imageWin[i].hBitmap = CreateCompatibleBitmap(hDC, X_SIZE, Y_SIZE);
  rect.top = 0;
  rect.left = 0;
  rect.bottom = rect.top + Y_SIZE;
  rect.right = rect.left + X_SIZE;
  AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);
  imageWin[WIN].hWnd =
      CreateWindow("ImageClass", wTitle[WIN], WS_OVERLAPPED, WIN_POSX, WIN_POSY,
                   rect.right - rect.left, rect.bottom - rect.top, NULL, NULL,
                   (HINSTANCE)hInstance, NULL);
  if (imageWin[WIN].hWnd == NULL) return FALSE;
  ShowWindow(imageWin[WIN].hWnd, nCmdShow);
  DisplayCImage(image_in, WIN);
  imageWin[WOUT].hWnd =
      CreateWindow("ImageClass", wTitle[WOUT], WS_OVERLAPPED, WOUT_POSX,
                   WOUT_POSY, rect.right - rect.left, rect.bottom - rect.top,
                   NULL, NULL, (HINSTANCE)hInstance, NULL);
  if (imageWin[WOUT].hWnd == NULL) return FALSE;
  ShowWindow(imageWin[WOUT].hWnd, nCmdShow);
  DisplayCImage(image_out, WOUT);
  imageWin[WWORK].hWnd =
      CreateWindow("ImageClass", wTitle[WWORK], WS_OVERLAPPED, WWORK_POSX,
                   WWORK_POSY, rect.right - rect.left, rect.bottom - rect.top,
                   NULL, NULL, (HINSTANCE)hInstance, NULL);
  if (imageWin[WWORK].hWnd == NULL) return FALSE;
  ShowWindow(imageWin[WWORK].hWnd, nCmdShow);
  DisplayCImage(image_work, WWORK);
  imageWin[WKEY].hWnd =
      CreateWindow("ImageClass", wTitle[WKEY], WS_OVERLAPPED, WKEY_POSX,
                   WKEY_POSY, rect.right - rect.left, rect.bottom - rect.top,
                   NULL, NULL, (HINSTANCE)hInstance, NULL);
  if (imageWin[WKEY].hWnd == NULL) return FALSE;
  ShowWindow(imageWin[WKEY].hWnd, nCmdShow);
  DisplayCImage(image_key, WKEY);
  for (i = 0; i < Y_SIZE; i++)
    for (j = 0; j < X_SIZE; j++) image_black[i][j] = 0;

  /* OPENFILENAME構造体のメンバを設定 */
  ofn.lStructSize = sizeof(OPENFILENAME);
  ofn.hwndOwner = hWnd;
  ofn.lpstrFilter = szFilterSpec;
  ofn.lpstrCustomFilter = NULL;
  ofn.nMaxCustFilter = 0;
  ofn.nFilterIndex = 1;
  ofn.lpstrFile = szFileName;
  ofn.nMaxFile = MAX_NAME_LENGTH;
  ofn.lpstrInitialDir = NULL;
  ofn.lpstrFileTitle = szFileTitle;
  ofn.nMaxFileTitle = MAX_NAME_LENGTH;
  ofn.lpstrTitle = NULL;
  ofn.lpstrDefExt = NULL;
  ofn.Flags = 0;
  return TRUE;
}

// AREA_DETECTのスレッド化ためし　2009.01.08.Y
// AREA_DETECTのスレッド化ためし　2009.01.08.Y
void AREA_DETECT_Thread_White() {

  init_cam_usb_std();
  while (g_bRun) {
    grab_cam_usb_std(image_in);
    image_copy_color(image_in, image_key);
    toGrayScale(image_key);
    thresh_gray(image_key[0], image_work[0], bar_two, bar_one, 4);
    toGrayScaleOld(image_work);

    labelingx(image_work[0], image_out[0], 0, 0, 255);
    toGrayScaleOld(image_out);

    featurex(image_out[0], image_in[0], 255);

    DisplayCImageFast(image_in, WIN);
    DisplayCImageFast(image_work, WWORK);
    DisplayCImageFast(image_out, WOUT);
    DisplayCImageFast(image_key, WKEY);
    Wait(10);
  }
  close_cam_usb_std();
}

// StartThreadためし　2009.01.08.Y
void StartThread() {
  // スレッド起動中
  if (g_bRun != FALSE) return;
  // 各スレッド作成
  g_bRun = TRUE;
  hThread = CreateThread(
      0, 0, (LPTHREAD_START_ROUTINE)AREA_DETECT_Thread_White, 0, 0, &dThreadID);
}

// EndThreadのためし　2009.01.08.Y
void EndThread() { g_bRun = FALSE; }

