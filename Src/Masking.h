#ifndef __INCLUDE_MASKING_H__
#define __INCLUDE_MASKING_H__

#include "Params.h"

void masking(unsigned char image_in[Y_SIZE][X_SIZE],
             unsigned char image_out[Y_SIZE][X_SIZE],
             unsigned char image_mask[Y_SIZE][X_SIZE]);

#endif
