#ifndef __INCLUDE_DILATION_H__
#define __INCLUDE_DILATION_H__

#include "Params.h"

void dilation(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_out[Y_SIZE][X_SIZE]);

#endif
