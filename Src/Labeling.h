#ifndef __INCLUDE_LABELING_H__
#define __INCLUDE_LABELING_H__

#include "Params.h"

int labelingx(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_label[Y_SIZE][X_SIZE], int v, int dest,
              int undest);

#endif
