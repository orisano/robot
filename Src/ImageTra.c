#include "ImageTra.h"
#include <stdio.h>
#include <stdlib.h>


/*--- rgb_read --- RGB面順次形式のカラー画像をディスクから読み込む ------------
        image:		カラー画像配列
        xsize:		画像横サイズ
        ysize:		画像縦サイズ
        filename:	ファイル名（.rgb）
-----------------------------------------------------------------------------*/
int rgb_read(unsigned char *image, int xsize, int ysize, char *filename) {
  FILE *fp;
  char fname_rgb[128];

  sprintf(fname_rgb, "%s.rgb", filename);
  if ((fp = fopen(fname_rgb, "rb")) == NULL) return -1;
  fread(image, sizeof(unsigned char), (size_t)(long)xsize * ysize * 3, fp);
  fclose(fp);

  return 0;
}

/*--- rgb_write --- カラー画像をディスクにRGB面順次形式で書き出す -------------
        image:		カラー画像配列
        xsize:		画像横サイズ
        ysize:		画像縦サイズ
        filename:	ファイル名（.rgb）
-----------------------------------------------------------------------------*/
int rgb_write(unsigned char *image, int xsize, int ysize, char *filename) {
  FILE *fp;
  char fname_rgb[128];

  sprintf(fname_rgb, "%s.rgb", filename);
  if ((fp = fopen(fname_rgb, "wb")) == NULL) return -1;
  fwrite(image, sizeof(unsigned char), (size_t)(long)xsize * ysize * 3, fp);
  fclose(fp);

  return 0;
}

/*--- bmp_read --- BMP形式のカラー画像をディスクから読み込む ------------------
        image:		カラー画像配列
        xsize:		画像横サイズ
        ysize:		画像縦サイズ
        filename:	ファイル名（.bmp）
-----------------------------------------------------------------------------*/
int bmp_read(unsigned char *image, int xsize, int ysize, char *filename) {
  long i, j;
  FILE *fp;
  unsigned char *image_buf;
  //	char fname_bmp[128]; 2004.7.13.
  char fname_bmp[128];
  unsigned char header[54];

  image_buf = (unsigned char *)malloc((size_t)xsize * ysize * 3);
  if (image_buf == NULL) return -1;

  if ((fp = fopen(filename, "rb")) == NULL) return -1;

  fread(header, sizeof(unsigned char), 54, fp);
  fread(image_buf, sizeof(unsigned char), (size_t)(long)xsize * ysize * 3, fp);
  fclose(fp);

  for (i = 0; i < ysize; i++) {
    for (j = 0; j < xsize; j++) {
      *(image + xsize *(ysize - i - 1) + j) =
          *(image_buf + 3 * (xsize * i + j) + 2);
      *(image + xsize *(ysize - i - 1) + j + xsize *ysize) =
          *(image_buf + 3 * (xsize * i + j) + 1);
      *(image + xsize *(ysize - i - 1) + j + xsize *ysize * 2) =
          *(image_buf + 3 * (xsize * i + j));
    }
  }

  free(image_buf);

  return 0;
}

/*--- bmp_write --- カラー画像をディスクにBMP形式で書き出す -------------------
        image:		カラー画像配列
        xsize:		画像横サイズ
        ysize:		画像縦サイズ
        filename:	ファイル名（.bmp）
-----------------------------------------------------------------------------*/
int bmp_write(unsigned char *image, int xsize, int ysize, char *filename) {
  long i, j;
  FILE *fp;
  long file_size, width, height;
  unsigned char *image_buf;
  char fname_bmp[128];
  unsigned char header[54] = {0x42, 0x4d, 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0,
                              40,   0,    0, 0, 0, 0, 0, 0, 0, 0, 0,  0, 1, 0,
                              24,   0,    0, 0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0,
                              0,    0,    0, 0, 0, 0, 0, 0, 0, 0, 0,  0};

  file_size = (long)xsize * (long)ysize * 3 + 54;
  header[2] = (unsigned char)(file_size & 0x000000ff);
  header[3] = (file_size >> 8) & 0x000000ff;
  header[4] = (file_size >> 16) & 0x000000ff;
  header[5] = (file_size >> 24) & 0x000000ff;
  width = xsize;
  header[18] = width & 0x000000ff;
  header[19] = (width >> 8) & 0x000000ff;
  header[20] = (width >> 16) & 0x000000ff;
  header[21] = (width >> 24) & 0x000000ff;
  height = ysize;
  header[22] = height & 0x000000ff;
  header[23] = (height >> 8) & 0x000000ff;
  header[24] = (height >> 16) & 0x000000ff;
  header[25] = (height >> 24) & 0x000000ff;

  image_buf = (unsigned char *)malloc((size_t)xsize * ysize * 3);
  if (image_buf == NULL) return -1;

  for (i = 0; i < ysize; i++) {
    for (j = 0; j < xsize; j++) {
      *(image_buf + 3 *(xsize *i + j)) =
          *(image + xsize * (ysize - i - 1) + j + (long)xsize * ysize * 2);
      *(image_buf + 3 *(xsize *i + j) + 1) =
          *(image + xsize * (ysize - i - 1) + j + (long)xsize * ysize);
      *(image_buf + 3 *(xsize *i + j) + 2) =
          *(image + xsize * (ysize - i - 1) + j);
    }
  }

  sprintf(fname_bmp, "%s.bmp", filename);
  if ((fp = fopen(fname_bmp, "wb")) == NULL) return -1;
  fwrite(header, sizeof(unsigned char), 54, fp);
  fwrite(image_buf, sizeof(unsigned char), (size_t)(long)xsize * ysize * 3, fp);
  fclose(fp);

  free(image_buf);

  return 0;
}
/*--- bmp_read2 --- BMP形式のカラー画像をディスクから読み込む ------------------
        image:		カラー画像配列
        xsize:		画像横サイズ
        ysize:		画像縦サイズ
        filename:	ファイル名  .bmp付き
-----------------------------------------------------------------------------*/
int bmp_read2(unsigned char *image, int xsize, int ysize, char *filename) {
  long i, j;
  FILE *fp;
  unsigned char *image_buf;
  char fname_bmp[128];
  unsigned char header[54];

  image_buf = (unsigned char *)malloc((size_t)xsize * ysize * 3);
  if (image_buf == NULL) return -1;

  if ((fp = fopen(filename, "rb")) == NULL) return -1;
  fread(header, sizeof(unsigned char), 54, fp);
  fread(image_buf, sizeof(unsigned char), (size_t)(long)xsize * ysize * 3, fp);
  fclose(fp);

  for (i = 0; i < ysize; i++) {
    for (j = 0; j < xsize; j++) {
      *(image + xsize *(ysize - i - 1) + j) =
          *(image_buf + 3 * (xsize * i + j) + 2);
      *(image + xsize *(ysize - i - 1) + j + xsize *ysize) =
          *(image_buf + 3 * (xsize * i + j) + 1);
      *(image + xsize *(ysize - i - 1) + j + xsize *ysize * 2) =
          *(image_buf + 3 * (xsize * i + j));
    }
  }

  free(image_buf);

  return 0;
}
