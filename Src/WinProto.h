#ifndef _WINPROTO_H
#define _WINPROTO_H
void BitmapToRgb(HBITMAP hBitmap, unsigned char pr[Y_SIZE][X_SIZE],
                 unsigned char pg[Y_SIZE][X_SIZE],
                 unsigned char pb[Y_SIZE][X_SIZE]);
void DisplayCImage(unsigned char image[3][Y_SIZE][X_SIZE], int position);
void DisplayCImageFast(unsigned char image[3][Y_SIZE][X_SIZE], int position);
void DisplayImage(unsigned char image[Y_SIZE][X_SIZE], int position);
BOOL GetParams1(char *szQuestion, float *param1);
BOOL GetParams2(char *szQuestion, char *szQuestion1, char *szQuestion2,
                float *param1, float *param2);
BOOL GetParams3(char *szQuestion, char *szQuestion1, char *szQuestion2,
                char *szQuestion3, float *param1, float *param2, float *param3);
BOOL GetParams4(char *szQuestion, char *szQuestion1, char *szQuestion2,
                char *szQuestion3, char *szQuestion4, float *param1,
                float *param2, float *param3, float *param4);
BOOL GetSelection2(char *szQuestion, char *szQuestion1, char *szQuestion2,
                   int *param);
BOOL GetSelection3(char *szQuestion, char *szQuestion1, char *szQuestion2,
                   char *szQuestion3, int *param);
int ImgPrint(unsigned char pr[P_Y_SIZE][P_X_SIZE],
             unsigned char pg[P_Y_SIZE][P_X_SIZE],
             unsigned char pb[P_Y_SIZE][P_X_SIZE]);
void m_dither(unsigned char image_in[P_Y_SIZE * 4][P_X_SIZE],
              unsigned char image_out[P_Y_SIZE][P_X_SIZE]);
void o_dither(unsigned char image_in[P_Y_SIZE][X_SIZE],
              unsigned char image_out[P_Y_SIZE][P_X_SIZE]);
void RgbToBitmap(unsigned char pr[Y_SIZE][X_SIZE],
                 unsigned char pg[Y_SIZE][X_SIZE],
                 unsigned char pb[Y_SIZE][X_SIZE], HBITMAP hBitmap);
void Wait(int wait_ms);

#endif