#include "ardrone/ardrone.h"
#include "cv_drone.h"  //20130701
#include "Params.h"    //20130704
#define KEY_DOWN(key) (GetAsyncKeyState(key) & 0x8000)
#define KEY_PUSH(key) (GetAsyncKeyState(key) & 0x0001)

extern "C" {
  int cv_drone(void);
}

// --------------------------------------------------------------------------
// main(引数の数、引数リスト)
// メイン関数です
// 戻り値 正常終了:0 エラー:-1
// --------------------------------------------------------------------------

ARDrone ardrone;  // 20130628

// void cv_drone_takeoff(void);20130701
// void cv_drone_landing(void);20130701
// void cv_drone_setCamera(void);20130701

int cv_drone(void)
// int main(int argc, char **argv)
{
  // AR.Droneクラス
  // ARDrone ardrone;20130628

  // 初期化
  if (!ardrone.open()) {
    printf("AR.Droneの初期化に失敗しました\n");
    return -1;
  }

  // メインループ
  while (!GetAsyncKeyState(VK_ESCAPE)) {
    // 更新
    if (!ardrone.update()) break;

    // 画像の取得
    IplImage *image = ardrone.getImage();

    // ナビゲーションデータの取得
    double roll = ardrone.getRoll();
    double pitch = ardrone.getPitch();
    double yaw = ardrone.getYaw();
    double altitude = ardrone.getAltitude();
    double vx, vy, vz;
    double velocity = ardrone.getVelocity(&vx, &vy, &vz);
    int battery = ardrone.getBatteryPercentage();

    if (KEY_PUSH(VK_SPACE)) {
      if (ardrone.onGround())
        cv_drone_takeoff();
      else
        cv_drone_landing();
    }

    double x = 0.0, y = 0.0, z = 0.0, r = 0.0;
    if (KEY_DOWN(VK_UP)) x = 0.5;
    if (KEY_DOWN(VK_DOWN)) x = -0.5;
    if (KEY_DOWN(VK_LEFT)) y = 0.5;
    if (KEY_DOWN(VK_RIGHT)) y = -0.5;
    if (KEY_DOWN('Q')) z = 0.5;
    if (KEY_DOWN('A')) z = -0.5;
    ardrone.move3D(x, y, z, r);

    // カメラ切り替え
    static int mode = 0;
    if (KEY_PUSH('C')) ardrone.setCamera(++mode % 4);

    // 表示
    cvShowImage("camera", image);
    cvWaitKey(1);
  }

  // さようなら
  ardrone.close();

  return 0;
}
///* 20130628
void cv_drone_takeoff(void) { ardrone.takeoff(); }

void cv_drone_landing(void) { ardrone.landing(); }

void cv_drone_setCamera(void)  // 20130701
{
  // 初期化 20130701
  if (!ardrone.open()) {
    printf("AR.Droneの初期化に失敗しました\n");
    return;  // 20130701
  }

  static int mode = 0;
  if (KEY_PUSH('C')) ardrone.setCamera(++mode % 4);

  ardrone.close();
}

void cv_drone_cam_in(unsigned char img_out[3][Y_SIZE][X_SIZE])
{
  ardrone.setCamera(mode);
  IplToArray(ardrone.getImage(), img_out);
}

void cv_drone_init(void)  // 20130703
{
  // 初期化 20130701
  if (!ardrone.open()) {
    printf("AR.Droneの初期化に失敗しました\n");
    return;  // 20130701
  }
}
void cv_drone_close(void)  // 20130703
{
  // さようなら 20130701
  ardrone.close();
}
