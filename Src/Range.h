#ifndef __INCLUDE_RANGE_H__
#define __INCLUDE_RANGE_H__

#include "Params.h"

void range(unsigned char image_in[Y_SIZE][X_SIZE], int *fmax, int *fmin);

#endif
