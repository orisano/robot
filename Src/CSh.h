#ifndef __INCLUDE_CSH_H__
#define __INCLUDE_CSH_H__

#include "Params.h"

void c_to_sh(int c1[Y_SIZE][X_SIZE], int c2[Y_SIZE][X_SIZE],
             int sat[Y_SIZE][X_SIZE], int hue[Y_SIZE][X_SIZE]);
void sh_to_c(int sat[Y_SIZE][X_SIZE], int hue[Y_SIZE][X_SIZE],
             int c1[Y_SIZE][X_SIZE], int c2[Y_SIZE][X_SIZE]);

#endif
