#include "Params.h"
#include "Proto.h"

#include <stdlib.h>

int calc_center_of_gravity(unsigned char image_label[Y_SIZE][X_SIZE], int label,
                           int *cx, int *cy);
int draw_line(unsigned char img_in[Y_SIZE][X_SIZE], int x0, int y0, int x1,
              int y1, int level);
int draw_mark(unsigned char canvas[Y_SIZE][X_SIZE], int x, int y, int wide, int level);

/**
 * ラベリング処理が終わり最大面積以外のラベルが排除されたデータに対して
 * 最大面積のラベルの重心を求め、重心位置にカーソルを描画する関数
 *
 * @param image_label_in 最大面積以外のラベルが排除されたラベル画像データ
 * @param image_label_out 重心位置にカーソルを表示させたい画像データ
 * @param label 最大面積のラベル番号
 *
 */
void featurex(unsigned char image_label_in[Y_SIZE][X_SIZE],
              unsigned char image_label_out[Y_SIZE][X_SIZE], int label) {
  int max_center_x, max_center_y;

  calc_center_of_gravity(image_label_in, label, &max_center_x, &max_center_y);
  draw_mark(image_label_out, max_center_x, max_center_y, 10, 0);
}

/**
 * 重心を求める関数
 * 画像の重心の求め方については以下を参照
 * 参考URL: http://www.gifu-nct.ac.jp/elec/yamada/iwata/cyu/
 *
 * @param image_label ラベル画像データ
 * @param label 重心を求めたいラベル番号
 * @param cx 重心のx座標が格納される。
 * @param cy 重心のy座標が格納される。
 * @return 重心を求めることに成功した場合は0,失敗した場合は1を返却する。
 *
 * 重心を求めることに失敗するのはラベル画像データの中に指定したラベル番号が存在しない場合
 * 失敗した場合cxとcyには-1が代入されるため、戻りのチェックを行わなければ配列外アクセスを引き起こす
 *
 */
int calc_center_of_gravity(unsigned char image_label[Y_SIZE][X_SIZE], int label,
                           int *cx, int *cy) {
  int i, j;
  int tx, ty, total;
  tx = ty = total = 0;

  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      if (image_label[i][j] == label) {
        tx += j;
        ty += i;
        total++;
      }
    }
  }

  if (total == 0) {
    *cx = *cy = -1;
    return (1);
  }

  *cx = tx / total;
  *cy = ty / total;

  return (0);
}

/**
 * 2点間に線分を描画する関数
 * Bresenham's line algorithm を用いた実装
 * 参考URL:
 *http://ja.wikipedia.org/wiki/%E3%83%96%E3%83%AC%E3%82%BC%E3%83%B3%E3%83%8F%E3%83%A0%E3%81%AE%E3%82%A2%E3%83%AB%E3%82%B4%E3%83%AA%E3%82%BA%E3%83%A0
 *
 * @param canvas 線分を描画する先の画像データ
 * @param x0 線分の始点のx座標
 * @param y0 線分の始点のy座標
 * @param x1 線分の終点のx座標
 * @param y1 線分の終点のy座標
 * @param level 描画する線分のグレースケール値
 * @return 常に0を返す
 *
*/
int draw_line(unsigned char canvas[Y_SIZE][X_SIZE], int x0, int y0, int x1,
              int y1, int level) {
  int dx, dy;
  int sx, sy;
  int err, e2;

  dx = abs(x0 - x1);
  dy = abs(y0 - y1);
  err = dx - dy;
  sx = sy = 1;
  if (x0 > x1) sx *= -1;
  if (y0 > y1) sy *= -1;
  while (1) {
    canvas[y0][x0] = level;
    if (x0 == x1 && y0 == y1) {
      break;
    }
    e2 = err * 2;
    if (e2 > -dy) {
      err -= dy;
      x0 += sx;
    }
    if (e2 < dx) {
      err += dx;
      y0 += sy;
    }
  }
  return (0);
}

/**
 * 指定した座標にカーソルを描画する関数
 *
 * @param canvas カーソルを描画する先の画像データ
 * @param x カーソルを描画するx座標
 * @param y カーソルを描画するy座標
 * @param wide カーソルのpixel幅
 * @param level 描画するカーソルのグレースケール値
 * @return 常に0を返す
 *
 * 現在の実装では5pixelの太さのカーソルが描画される
*/
int draw_mark(unsigned char canvas[Y_SIZE][X_SIZE], int x, int y, int wide,
              int level) {
  int i;
  for (i = -2; i <= 2; i++) {
    draw_line(canvas, x + wide, y + i, x - wide, y + i, level);
    draw_line(canvas, x + i, y - wide, x + i, y + wide, level);
  }
  return (0);
}
