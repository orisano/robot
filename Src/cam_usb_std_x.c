//
// cam_usb_std_x.c	OpenCVを使った画像入力（連続入力版）
//
//                               2008.9.7. 姉崎
//                               2009.8.5. 修正
//  typedef unsigned char BYTE;  が必要  20080315
#include "cam_usb_std_x.h"
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>

static CvCapture* sCapture = NULL;

int init_cam_usb_std(void){
  //カメラを取得。引数はカメラ番号。
  // CV_CAP_ANY = 0 カメラなら何でも取得
  // capture = cvCaptureFromCAM(CV_CAP_ANY);
  if (sCapture != NULL){
    close_cam_usb_std();
  }
  sCapture = cvCaptureFromCAM(0);
  
  if (!sCapture) {
    return (1);
  }
  return (0);
}

int grab_cam_usb_std(unsigned char img_in[][Y_SIZE][X_SIZE]){
  const int offset_x = 50;
  IplImage* capimg;
  int height, width;
  const unsigned char* data;
  int base, x, y;
  int x_scale, y_scale;
  int gx, gy;

  if (sCapture == NULL){
    return (1);
  }

  //カメラから画像を取得
  capimg = cvQueryFrame(sCapture);
  height = capimg->height;
  width = capimg->width;
  data = (const unsigned char*)capimg->imageData;

  if (height == 240){
    x_scale = 1;
    y_scale = 1;
  }
  else {
    x_scale = 2; 
    y_scale = 2;
  }

  for (y = 0; y < height; y += y_scale) {
    for (x = 0; x < width; x += x_scale) {
      base = ((width - x) + y * width) * 3;
      gx = x / x_scale;
      gy = y / y_scale;

      if (offset_x < gx && gx < X_SIZE + offset_x) {
        img_in[0][gy][gx - offset_x] = data[base + 2]; // R
        img_in[1][gy][gx - offset_x] = data[base + 1]; // G
        img_in[2][gy][gx - offset_x] = data[base + 0]; // B
      }
    }
  }

  return (0);
}


int close_cam_usb_std(void){
  //カメラの開放
  cvReleaseCapture(&sCapture);
  sCapture = NULL;
  return (0);
}
