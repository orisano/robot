#ifndef __INCLUDE_FFT_CORE_H__
#define __INCLUDE_FFT_CORE_H__

#include "Params.h"

void fftcore(float a_rl[], float a_im[], int length, int ex, float sin_tbl[],
             float cos_tbl[], float buf[]);
void cstb(int length, int inv, float sin_tbl[], float cos_tbl[]);
void birv(float a[], int length, int ex, float b[]);
void rvmtx1(float a[Y_SIZE][X_SIZE], float b[X_SIZE][Y_SIZE], int xsize,
            int ysize);
void rvmtx2(float a[X_SIZE][Y_SIZE], float b[Y_SIZE][X_SIZE], int xsize,
            int ysize);

#endif
