#ifndef __INCLUDE_SHIFT_H__
#define __INCLUDE_SHIFT_H__

#include "Params.h"

void shift(unsigned char image_in[Y_SIZE][X_SIZE],
           unsigned char image_out[Y_SIZE][X_SIZE], float px, float py);

#endif
