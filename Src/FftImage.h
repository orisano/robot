#ifndef __INCLUDE_FFT_IMAGE_H__
#define __INCLUDE_FFT_IMAGE_H__

#include "Params.h"

int fftimage(unsigned char image_in[Y_SIZE][X_SIZE],
    unsigned char image_out[Y_SIZE][X_SIZE]);

#endif
