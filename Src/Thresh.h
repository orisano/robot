#ifndef __INCLUDE_THRESH_H__
#define __INCLUDE_THRESH_H__

#include "Params.h"

void threshold(unsigned char image_in[Y_SIZE][X_SIZE],
               unsigned char image_out[Y_SIZE][X_SIZE], int thresh, int mode);

void thresh_gray(unsigned char image_in[Y_SIZE][X_SIZE],
                 unsigned char image_out[Y_SIZE][X_SIZE], int thresh1,
                 int thresh2, int mode);

void thresh_3d(unsigned char image_in[Y_SIZE][X_SIZE],
               unsigned char image_out[Y_SIZE][X_SIZE], int thresh1,
               int thresh2);

void thresh_3d_1(unsigned char image_in[Y_SIZE][X_SIZE],
                 unsigned char image_out[Y_SIZE][X_SIZE], int thresh1,
                 int thresh2);

void thresh_full_view(unsigned char img_in[Y_SIZE][X_SIZE],
                      unsigned char img_out[Y_SIZE][X_SIZE], int thresh1,
                      int thresh2);

#endif
