#ifndef __INCLUDE_THRESH_C_H__
#define __INCLUDE_THRESH_C_H__

#include "Params.h"

void thresh_color(unsigned char image_in_r[Y_SIZE][X_SIZE],
                  unsigned char image_in_g[Y_SIZE][X_SIZE],
                  unsigned char image_in_b[Y_SIZE][X_SIZE],
                  unsigned char image_out_r[Y_SIZE][X_SIZE],
                  unsigned char image_out_g[Y_SIZE][X_SIZE],
                  unsigned char image_out_b[Y_SIZE][X_SIZE], int thdrl,
                  int thdrm, int thdgl, int thdgm, int thdbl, int thdbm);

#endif
