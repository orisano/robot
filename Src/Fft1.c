#include "Fft1.h"
#include "FftCore.h"
#include <stdlib.h>

/*--- fft1 --- １次元ＦＦＴの実行
---------------------------------------------
        a_rl:	データ実数部（入出力兼用）
        a_im:	データ虚数部（入出力兼用）
        ex:
データ個数を２のべき乗で与える(データ個数 =
2のex乗個）
        inv:	1: ＤＦＴ，-1: 逆ＤＦＴ
-----------------------------------------------------------------------------*/
int fft1(float a_rl[], float a_im[], int ex, int inv) {
  int i, length = 1;
  float *sin_tbl; /* SIN計算用テーブル　*/
  float *cos_tbl; /* COS計算用テーブル　*/
  float *buf;     /* 作業用バッファ　*/

  for (i = 0; i < ex; i++) length *= 2; /* データ個数の計算 */
  sin_tbl = (float *)malloc((size_t)length * sizeof(float));
  cos_tbl = (float *)malloc((size_t)length * sizeof(float));
  buf = (float *)malloc((size_t)length * sizeof(float));
  if ((sin_tbl == NULL) || (cos_tbl == NULL) || (buf == NULL)) {
    return (-1);
  }
  cstb(length, inv, sin_tbl, cos_tbl); /* SIN,COSテーブル作成 */
  fftcore(a_rl, a_im, length, ex, sin_tbl, cos_tbl, buf);
  free(sin_tbl);
  free(cos_tbl);
  return 0;
}

