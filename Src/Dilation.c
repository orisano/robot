#include "Dilation.h"

/*--- dilation --- 膨張処理 ---------------------------------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
-----------------------------------------------------------------------------*/
void dilation(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_out[Y_SIZE][X_SIZE]) {
  int i, j;

  for (i = 1; i < Y_SIZE - 1; i++) {
    for (j = 1; j < X_SIZE - 1; j++) {
      image_out[i][j] = image_in[i][j];
      if (image_in[i - 1][j - 1] == HIGH) image_out[i][j] = HIGH;
      if (image_in[i - 1][j] == HIGH) image_out[i][j] = HIGH;
      if (image_in[i - 1][j + 1] == HIGH) image_out[i][j] = HIGH;
      if (image_in[i][j - 1] == HIGH) image_out[i][j] = HIGH;
      if (image_in[i][j + 1] == HIGH) image_out[i][j] = HIGH;
      if (image_in[i + 1][j - 1] == HIGH) image_out[i][j] = HIGH;
      if (image_in[i + 1][j] == HIGH) image_out[i][j] = HIGH;
      if (image_in[i + 1][j + 1] == HIGH) image_out[i][j] = HIGH;
    }
  }
}
