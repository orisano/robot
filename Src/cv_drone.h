//
// cv_drone.h : ヘッダー ファイル
//
//    2013.7.1. 姉崎
//
#include "Params.h"  //20130704

int cv_drone(void);
void cv_drone_takeoff(void);
void cv_drone_landing(void);
void cv_drone_setCamera(void);
void cv_drone_init(void);
void cv_drone_close(void);
void cv_drone_cam_in(unsigned char img_out[3][Y_SIZE][X_SIZE]);
