#ifndef __INCLUDE_IMAGE_UTIL_H__
#define __INCLUDE_IMAGE_UTIL_H__

#include "Params.h"
#include <cxtypes.h>

void IplToArray(const IplImage* image_in,
                unsigned char image_out[3][Y_SIZE][X_SIZE]);
void toGrayScale(unsigned char image_out[3][Y_SIZE][X_SIZE]);
void toGrayScaleOld(unsigned char image_out[3][Y_SIZE][X_SIZE]);

#endif
