#include "WinParams.h"
#include "WinDialog.h"

BOOL CALLBACK DlgProc1(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProc2(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProc3(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProc4(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProcS2(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProcS3(HWND, UINT, WPARAM, LPARAM);

extern HANDLE ghInst;
extern HWND hGWnd;
int gMode;
char gQuestion[128] = "Question";
char gQuestion1[128] = "Question1";
char gQuestion2[128] = "Question2";
char gQuestion3[128] = "Question3";
char gQuestion4[128] = "Question4";
char gAnswer1[128] = "Answer1";
char gAnswer2[128] = "Answer2";
char gAnswer3[128] = "Answer3";
char gAnswer4[128] = "Answer4";

/*--- GetParams1 --- パラメータを得る -----------------------------------------
        szQuestion:		質問
        param1:			パラメータ１
-----------------------------------------------------------------------------*/
BOOL GetParams1(char *szQuestion, float *param1) {
  sprintf(gQuestion, szQuestion);
  if (!DialogBox(ghInst, (LPCTSTR)DIALOG_1, hGWnd, DlgProc1)) return FALSE;
  *param1 = (float)atof(gAnswer1);
  return TRUE;
}

/*--- GetParams2 --- パラメータを得る -----------------------------------------
        szQuestion:		質問
        szQuestion1:	質問
        szQuestion2:	質問
        param1:			パラメータ１
        param2:			パラメータ２
-----------------------------------------------------------------------------*/
BOOL GetParams2(char *szQuestion, char *szQuestion1, char *szQuestion2,
                float *param1, float *param2) {
  sprintf(gQuestion, szQuestion);
  sprintf(gQuestion1, szQuestion1);
  sprintf(gQuestion2, szQuestion2);
  if (!DialogBox(ghInst, (LPCTSTR)DIALOG_2, hGWnd, DlgProc2)) return FALSE;
  *param1 = (float)atof(gAnswer1);
  *param2 = (float)atof(gAnswer2);
  return TRUE;
}

/*--- GetParams3 --- パラメータを得る -----------------------------------------
        szQuestion:		質問
        szQuestion1:	質問
        szQuestion2:	質問
        szQuestion3:	質問
        param1:			パラメータ１
        param2:			パラメータ２
        param3:			パラメータ３
-----------------------------------------------------------------------------*/
BOOL GetParams3(char *szQuestion, char *szQuestion1, char *szQuestion2,
                char *szQuestion3, int *param1, int *param2, int *param3) {
  sprintf(gQuestion, szQuestion);
  sprintf(gQuestion1, szQuestion1);
  sprintf(gQuestion2, szQuestion2);
  sprintf(gQuestion3, szQuestion3);
  if (!DialogBox(ghInst, (LPCTSTR)DIALOG_3, hGWnd, DlgProc3)) return FALSE;
  *param1 = (float)atof(gAnswer1);
  *param2 = (float)atof(gAnswer2);
  *param3 = (float)atof(gAnswer3);
  return TRUE;
}

/*--- GetParams4 --- パラメータを得る -----------------------------------------
        szQuestion:		質問
        szQuestion1:	質問
        szQuestion2:	質問
        szQuestion3:	質問
        szQuestion4:	質問
        param1:			パラメータ１
        param2:			パラメータ２
        param3:			パラメータ３
        param4:			パラメータ4
-----------------------------------------------------------------------------*/
BOOL GetParams4(char *szQuestion, char *szQuestion1, char *szQuestion2,
                char *szQuestion3, char *szQuestion4, float *param1,
                float *param2, float *param3, float *param4) {
  sprintf(gQuestion, szQuestion);
  sprintf(gQuestion1, szQuestion1);
  sprintf(gQuestion2, szQuestion2);
  sprintf(gQuestion3, szQuestion3);
  sprintf(gQuestion3, szQuestion3);
  if (!DialogBox(ghInst, (LPCTSTR)DIALOG_4, hGWnd, DlgProc4)) return FALSE;
  *param1 = (float)atof(gAnswer1);
  *param2 = (float)atof(gAnswer2);
  *param3 = (float)atof(gAnswer3);
  *param4 = (float)atof(gAnswer4);
  return TRUE;
}

/*--- GetSelection2 --- 選択を処理する ----------------------------------------
        szQuestion:		質問
        szQuestion1:	選択１
        szQuestion2:	選択２
        param:			結果
-----------------------------------------------------------------------------*/
BOOL GetSelection2(char *szQuestion, char *szQuestion1, char *szQuestion2,
                   int *param) {
  sprintf(gQuestion, szQuestion);
  sprintf(gQuestion1, szQuestion1);
  sprintf(gQuestion2, szQuestion2);
  if (!DialogBox(ghInst, (LPCTSTR)DIALOG_S2, hGWnd, DlgProcS2)) return FALSE;
  *param = gMode;
  return TRUE;
}

/*--- GetSelection3 --- 選択を処理する ----------------------------------------
        szQuestion:		質問
        szQuestion1:	選択１
        szQuestion2:	選択２
        szQuestion3:	選択３
        param:			結果
-----------------------------------------------------------------------------*/
BOOL GetSelection3(char *szQuestion, char *szQuestion1, char *szQuestion2,
                   char *szQuestion3, int *param) {
  sprintf(gQuestion, szQuestion);
  sprintf(gQuestion1, szQuestion1);
  sprintf(gQuestion2, szQuestion2);
  sprintf(gQuestion3, szQuestion3);
  if (!DialogBox(ghInst, (LPCTSTR)DIALOG_S3, hGWnd, DlgProcS3)) return FALSE;
  *param = gMode;
  return TRUE;
}

/*--- DlgProc1 --- ダイアログを処理する ---------------------------------------
        hDlg:		ウィンドウ
        message:	メッセージ
        wParam:		ワードパラメータ
        lParam:		ロングパラメータ
-----------------------------------------------------------------------------*/
BOOL CALLBACK DlgProc1(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {

  switch (message) {
    case WM_INITDIALOG:
      SetDlgItemText(hDlg, IDC_EDIT1, (LPSTR)gQuestion);
      return TRUE;
    case WM_COMMAND:
      switch (wParam) {
        case IDOK:
          GetDlgItemText(hDlg, IDC_EDIT2, (LPSTR)gAnswer1, 16);
          EndDialog(hDlg, TRUE);
          return TRUE;
        case IDCANCEL:
          EndDialog(hDlg, FALSE);
          return FALSE;
        default:
          break;
      }
  }
  return FALSE;
}

/*--- DlgProc2 --- ダイアログを処理する ---------------------------------------
        hDlg:		ウィンドウ
        message:	メッセージ
        wParam:		ワードパラメータ
        lParam:		ロングパラメータ
-----------------------------------------------------------------------------*/
BOOL CALLBACK DlgProc2(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
  switch (message) {
    case WM_INITDIALOG:
      SetDlgItemText(hDlg, IDC_EDIT1, (LPSTR)gQuestion);
      SetDlgItemText(hDlg, IDC_EDIT2, (LPSTR)gQuestion1);
      SetDlgItemText(hDlg, IDC_EDIT4, (LPSTR)gQuestion2);
      return TRUE;
    case WM_COMMAND:
      switch (wParam) {
        case IDOK:
          GetDlgItemText(hDlg, IDC_EDIT3, (LPSTR)gAnswer1, 16);
          GetDlgItemText(hDlg, IDC_EDIT5, (LPSTR)gAnswer2, 16);
          EndDialog(hDlg, TRUE);
          return TRUE;
        case IDCANCEL:
          EndDialog(hDlg, FALSE);
          return FALSE;
        default:
          break;
      }
  }
  return FALSE;
}

/*--- DlgProc3 --- ダイアログを処理する ---------------------------------------
        hDlg:		ウィンドウ
        message:	メッセージ
        wParam:		ワードパラメータ
        lParam:		ロングパラメータ
-----------------------------------------------------------------------------*/
BOOL CALLBACK DlgProc3(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
  switch (message) {
    case WM_INITDIALOG:
      SetDlgItemText(hDlg, IDC_EDIT1, (LPSTR)gQuestion);
      SetDlgItemText(hDlg, IDC_EDIT2, (LPSTR)gQuestion1);
      SetDlgItemText(hDlg, IDC_EDIT4, (LPSTR)gQuestion2);
      SetDlgItemText(hDlg, IDC_EDIT6, (LPSTR)gQuestion3);
      SetDlgItemText(hDlg, IDC_EDIT8, (LPSTR)gQuestion4);
      return (TRUE);
    case WM_COMMAND:
      switch (wParam) {
        case IDOK:
          GetDlgItemText(hDlg, IDC_EDIT3, (LPSTR)gAnswer1, 16);
          GetDlgItemText(hDlg, IDC_EDIT5, (LPSTR)gAnswer2, 16);
          GetDlgItemText(hDlg, IDC_EDIT7, (LPSTR)gAnswer3, 16);
          GetDlgItemText(hDlg, IDC_EDIT9, (LPSTR)gAnswer4, 16);
          EndDialog(hDlg, TRUE);
          return TRUE;
        case IDCANCEL:
          EndDialog(hDlg, FALSE);
          return FALSE;
        default:
          break;
      }
  }
  return FALSE;
}

/*--- DlgProc4 --- ダイアログを処理する ---------------------------------------
        hDlg:		ウィンドウ
        message:	メッセージ
        wParam:		ワードパラメータ
        lParam:		ロングパラメータ
-----------------------------------------------------------------------------*/
BOOL CALLBACK DlgProc4(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
  switch (message) {
    case WM_INITDIALOG:
      SetDlgItemText(hDlg, IDC_EDIT1, (LPSTR)gQuestion);
      SetDlgItemText(hDlg, IDC_EDIT2, (LPSTR)gQuestion1);
      SetDlgItemText(hDlg, IDC_EDIT4, (LPSTR)gQuestion2);
      SetDlgItemText(hDlg, IDC_EDIT6, (LPSTR)gQuestion3);
      SetDlgItemText(hDlg, IDC_EDIT8, (LPSTR)gQuestion4);
      return (TRUE);
    case WM_COMMAND:
      switch (wParam) {
        case IDOK:
          GetDlgItemText(hDlg, IDC_EDIT3, (LPSTR)gAnswer1, 16);
          GetDlgItemText(hDlg, IDC_EDIT5, (LPSTR)gAnswer2, 16);
          GetDlgItemText(hDlg, IDC_EDIT7, (LPSTR)gAnswer3, 16);
          GetDlgItemText(hDlg, IDC_EDIT9, (LPSTR)gAnswer4, 16);
          EndDialog(hDlg, TRUE);
          return TRUE;
        case IDCANCEL:
          EndDialog(hDlg, FALSE);
          return FALSE;
        default:
          break;
      }
  }
  return FALSE;
}

/*--- DlgProcS2 --- ダイアログを処理する --------------------------------------
        hDlg:		ウィンドウ
        message:	メッセージ
        wParam:		ワードパラメータ
        lParam:		ロングパラメータ
-----------------------------------------------------------------------------*/
BOOL CALLBACK DlgProcS2(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
  switch (message) {
    case WM_INITDIALOG:
      SetDlgItemText(hDlg, IDC_EDIT1, (LPSTR)gQuestion);
      SetDlgItemText(hDlg, IDC_RADIO1, (LPSTR)gQuestion1);
      SetDlgItemText(hDlg, IDC_RADIO2, (LPSTR)gQuestion2);
      CheckRadioButton(hDlg, IDC_RADIO1, IDC_RADIO2, IDC_RADIO1);
      gMode = 1;
      return TRUE;
    case WM_COMMAND:
      switch (wParam) {
        case IDOK:
          if (IsDlgButtonChecked(hDlg, IDC_RADIO1)) gMode = 1;
          if (IsDlgButtonChecked(hDlg, IDC_RADIO2)) gMode = 2;
          EndDialog(hDlg, TRUE);
          return TRUE;
        case IDCANCEL:
          EndDialog(hDlg, FALSE);
          return FALSE;
        default:
          break;
      }
  }
  return FALSE;
}

/*--- DlgProcS3 --- ダイアログを処理する --------------------------------------
        hDlg:		ウィンドウ
        message:	メッセージ
        wParam:		ワードパラメータ
        lParam:		ロングパラメータ
-----------------------------------------------------------------------------*/
BOOL CALLBACK DlgProcS3(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
  switch (message) {
    case WM_INITDIALOG:
      SetDlgItemText(hDlg, IDC_EDIT1, (LPSTR)gQuestion);
      SetDlgItemText(hDlg, IDC_RADIO1, (LPSTR)gQuestion1);
      SetDlgItemText(hDlg, IDC_RADIO2, (LPSTR)gQuestion2);
      SetDlgItemText(hDlg, IDC_RADIO3, (LPSTR)gQuestion3);
      CheckRadioButton(hDlg, IDC_RADIO1, IDC_RADIO3, IDC_RADIO1);
      gMode = 1;
      return TRUE;
    case WM_COMMAND:
      switch (wParam) {
        case IDOK:
          if (IsDlgButtonChecked(hDlg, IDC_RADIO1)) gMode = 1;
          if (IsDlgButtonChecked(hDlg, IDC_RADIO2)) gMode = 2;
          if (IsDlgButtonChecked(hDlg, IDC_RADIO3)) gMode = 3;
          EndDialog(hDlg, TRUE);
          return TRUE;
        case IDCANCEL:
          EndDialog(hDlg, FALSE);
          return FALSE;
        default:
          break;
      }
  }
  return FALSE;
}
