#ifndef __INCLUDE_ROTATION_H__
#define __INCLUDE_ROTATION_H__

#include "Params.h"

void rotation(unsigned char image_in[Y_SIZE][X_SIZE],
              unsigned char image_out[Y_SIZE][X_SIZE], float deg);

#endif
