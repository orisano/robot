#ifndef __INCLUDE_CAM_USB_STD_X_H__
#define __INCLUDE_CAM_USB_STD_X_H__

#include "Params.h"

int init_cam_usb_std(void);
int grab_cam_usb_std(unsigned char img_in[][Y_SIZE][X_SIZE]);
int close_cam_usb_std(void);

#endif