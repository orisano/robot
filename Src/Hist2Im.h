#ifndef __INCLUDE_HIST2_IM_H__
#define __INCLUDE_HIST2_IM_H__

#include "Params.h"

void hist2_image(unsigned char image_in1[Y_SIZE][X_SIZE],
                 unsigned char image_in2[Y_SIZE][X_SIZE],
                 unsigned char image_hist[Y_SIZE][X_SIZE]);

#endif
