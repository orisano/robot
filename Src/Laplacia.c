#include "Laplacia.h"

/*--- laplacian --- ２次微分による輪郭抽出 ------------------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        amp:		出力画像の利得
-----------------------------------------------------------------------------*/
void laplacian(unsigned char image_in[Y_SIZE][X_SIZE],
               unsigned char image_out[Y_SIZE][X_SIZE], float amp) {
  static int c[9] = {-1, -1, -1,  /* オペレータの係数(laplacian) */
                     -1, 8,  -1,  /* 他のオペレータ使用時は      */
                     -1, -1, -1}; /* 書き換えて下さい            */
  int d[9];
  int i, j, dat;
  float z, zz;

  for (i = 1; i < Y_SIZE - 1; i++) {
    for (j = 1; j < X_SIZE - 1; j++) {
      d[0] = image_in[i - 1][j - 1];
      d[1] = image_in[i - 1][j];
      d[2] = image_in[i - 1][j + 1];
      d[3] = image_in[i][j - 1];
      d[4] = image_in[i][j];
      d[5] = image_in[i][j + 1];
      d[6] = image_in[i + 1][j - 1];
      d[7] = image_in[i + 1][j];
      d[8] = image_in[i + 1][j + 1];
      z = (float)(c[0] * d[0] + c[1] * d[1] + c[2] * d[2] + c[3] * d[3] +
                  c[4] * d[4] + c[5] * d[5] + c[6] * d[6] + c[7] * d[7] +
                  c[8] * d[8]);
      zz = amp * z;
      dat = (int)(zz);
      if (dat < 0) dat = -dat;
      if (dat > 255) dat = 255;
      image_out[i][j] = (unsigned char)dat;
    }
  }
}

/*--- filter1 --- ２次微分による輪郭抽出 ------------------------------------
        img_in:	入力画像配列
        img_out:	出力画像配列
        amp:		出力画像の利得
-----------------------------------------------------------------------------*/
void filter1(unsigned char img_in[Y_SIZE][X_SIZE],
             unsigned char img_out[Y_SIZE][X_SIZE], float amp) {
  static int c[9] = {-1, 0, 1,  /* オペレータの係数(laplacian) */
                     -1, 0, 1,  /* 他のオペレータ使用時は      */
                     -1, 0, 1}; /* 書き換えて下さい            */
  int d[9];
  int i, j, dat;
  float z, zz;
  int hist[2][256], hista;

  // 画像とヒストの初期化　2004.9.15.
  for (i = 0; i < 256; i++) {
    for (j = 0; j < 256; j++) {
      img_out[j][i] = 0;
    }
  }
  for (i = 0; i < 256; i++) {
    hist[1][i] = 0;
    hist[0][i] = 0;
  }

  for (i = 1; i < Y_SIZE - 1; i++) {
    for (j = 1; j < X_SIZE - 1; j++) {
      d[0] = img_in[i - 1][j - 1];
      d[1] = img_in[i - 1][j];
      d[2] = img_in[i - 1][j + 1];
      d[3] = img_in[i][j - 1];
      d[4] = img_in[i][j];
      d[5] = img_in[i][j + 1];
      d[6] = img_in[i + 1][j - 1];
      d[7] = img_in[i + 1][j];
      d[8] = img_in[i + 1][j + 1];
      z = (float)(c[0] * d[0] + c[1] * d[1] + c[2] * d[2] + c[3] * d[3] +
                  c[4] * d[4] + c[5] * d[5] + c[6] * d[6] + c[7] * d[7] +
                  c[8] * d[8]);
      zz = amp * z;
      dat = (int)(zz);
      if (dat < 0) dat = -dat;
      if (dat > 255) dat = 255;
      img_out[i][j] = (unsigned char)dat;
    }
  }
  /* Panorama3 のSobelタテ線強調画像　のヒストグラム作成　2005.10.03. */

  for (i = 0; i < X_SIZE - 1; i++) {
    for (j = 0; j < Y_SIZE - 1; j++) {
      hista = img_out[j][i];
      hist[1][j] = hist[1][j] + hista;
      hist[0][i] = hist[0][i] + hista;
    }
  }

  for (i = 0; i < 198; i++) {
    hista = hist[0][i] / 200;
  }
  for (j = 15; j < Y_SIZE - 1; j++) {
    hista = hist[1][j] / 200;
  }
}
