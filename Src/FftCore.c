#include "FftCore.h"
#include <math.h>

#define OPT

/*--- fftcore --- ツ１ツ篠淞個ウツＦツＦツＴツづ個計ツ算ツづ個核ツづ可づ按づゥツ閉板閉ェ
---------------------------
        a_rl:	ツデツーツタツ偲青板閉板（ツ禿シツ出ツ療債個督用ツ）
        a_im:	ツデツーツタツ仰閉青板閉板（ツ禿シツ出ツ療債個督用ツ）
        ex:
ツデツーツタツ古つ青板づーツ２ツづ個づ猟つォツ湘ヲツづ与ツつヲツづゥ(ツデツーツタツ古つ青=
2ツづ憩xツ湘ヲツ古つ）
        sin_tbl:	SINツ計ツ算ツ用ツテツーツブツδ        cos_tbl:	COSツ計ツ算ツ用ツテツーツブツδ-----------------------------------------------------------------------------*/
void fftcore(float a_rl[], float a_im[], int length, int ex, float sin_tbl[],
             float cos_tbl[], float buf[]) {
  int i, j, k, w, j1, j2;
  int numb, lenb, timb;
  float xr, xi, yr, yi, nrml;

#ifdef OPT
  for (i = 1; i < length; i += 2) {
    a_rl[i] = -a_rl[i];
    a_im[i] = -a_im[i];
  }
#endif
  numb = 1;
  lenb = length;
  for (i = 0; i < ex; i++) {
    lenb /= 2;
    timb = 0;
    for (j = 0; j < numb; j++) {
      w = 0;
      for (k = 0; k < lenb; k++) {
        j1 = timb + k;
        j2 = j1 + lenb;
        xr = a_rl[j1];
        xi = a_im[j1];
        yr = a_rl[j2];
        yi = a_im[j2];
        a_rl[j1] = xr + yr;
        a_im[j1] = xi + yi;
        xr = xr - yr;
        xi = xi - yi;
        a_rl[j2] = xr * cos_tbl[w] - xi * sin_tbl[w];
        a_im[j2] = xr * sin_tbl[w] + xi * cos_tbl[w];
        w += numb;
      }
      timb += (2 * lenb);
    }
    numb *= 2;
  }
  birv(a_rl, length, ex, buf); /*ツ　ツ偲青板デツーツタツづ個陛づ猟環キツつヲツ　*/
  birv(a_im, length, ex, buf); /*ツ　ツ仰閉青板デツーツタツづ個陛づ猟環キツつヲツ　*/
#ifdef OPT
  for (i = 1; i < length; i += 2) {
    a_rl[i] = -a_rl[i];
    a_im[i] = -a_im[i];
  }
#endif
  nrml = (float)(1.0 / sqrt((float)length));
  for (i = 0; i < length; i++) {
    a_rl[i] *= nrml;
    a_im[i] *= nrml;
  }
}
/*--- cstb --- SIN,COSツテツーツブツδ仰催ャツ青ャ
--------------------------------------------
        length:		ツデツーツタツ古つ青        inv:		1: ツＤツＦツＴ, -1: ツ逆ツＤツＦツＴ
        sin_tbl:	SINツ計ツ算ツ用ツテツーツブツδ        cos_tbl:	COSツ計ツ算ツ用ツテツーツブツδ-----------------------------------------------------------------------------*/
void cstb(int length, int inv, float sin_tbl[], float cos_tbl[]) {
  int i;
  float xx, arg;

  xx = (float)(((-M_PI) * 2.0) / (float)length);
  if (inv < 0) xx = -xx;
  for (i = 0; i < length; i++) {
    arg = (float)i * xx;
    sin_tbl[i] = (float)sin(arg);
    cos_tbl[i] = (float)cos(arg);
  }
}

/*--- birv --- ツデツーツタツづ個陛づ猟環キツつヲ
-----------------------------------------------
        a:		ツデツーツタツづ個配ツ療ア
        length:	ツデツーツタツ古つ青        ex:
ツデツーツタツ古つ青板づーツ２ツづ個づ猟つォツ湘ヲツづ与ツつヲツづゥ(length
= 2ツづ憩xツ湘ヲツ古つ）
        b:		ツ催ャツ凝用ツバツッツフツァ
-----------------------------------------------------------------------------*/
void birv(float a[], int length, int ex, float b[]) {
  int i, ii, k, bit;

  for (i = 0; i < length; i++) {
    for (k = 0, ii = i, bit = 0;; bit <<= 1, ii >>= 1) {
      bit = (ii & 1) | bit;
      if (++k == ex) break;
    }
    b[i] = a[bit];
  }
  for (i = 0; i < length; i++) a[i] = b[i];
}

/*--- rvmtx1 --- ツ２ツ篠淞個ウツデツーツタツづ個転ツ置
-------------------------------------------
        a:		ツ２ツ篠淞個ウツ禿シツ療債デツーツタ
        b:		ツ２ツ篠淞個ウツ出ツ療債デツーツタ
        xsize:	ツ青閉スツデツーツタツ古つ青        ysize:	ツ青つ陳シツデツーツタツ古つ青-----------------------------------------------------------------------------*/
void rvmtx1(float a[Y_SIZE][X_SIZE], float b[X_SIZE][Y_SIZE], int xsize,
            int ysize) {
  int i, j;

  for (i = 0; i < ysize; i++) {
    for (j = 0; j < xsize; j++) b[j][i] = a[i][j];
  }
}

/*--- rvmtx2 --- ツ２ツ篠淞個ウツデツーツタツづ個転ツ置
-------------------------------------------
        a:		ツ２ツ篠淞個ウツ禿シツ療債デツーツタ
        b:		ツ２ツ篠淞個ウツ出ツ療債デツーツタ
        xsize:	ツ青閉スツデツーツタツ古つ青        ysize:	ツ青つ陳シツデツーツタツ古つ青-----------------------------------------------------------------------------*/
void rvmtx2(float a[X_SIZE][Y_SIZE], float b[Y_SIZE][X_SIZE], int xsize,
            int ysize) {
  int i, j;

  for (i = 0; i < ysize; i++) {
    for (j = 0; j < xsize; j++) b[i][j] = a[j][i];
  }
}

