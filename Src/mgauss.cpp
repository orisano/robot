#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#define sqr(x) ((x) * (x))

#define DDD 1.0e-4

double gauss(double x, double mean, double var);
double mixtured_gauss(double x, double *alpha, double *mean, double *var,
                      int c);

//１次元混合ガウス分布のパラメータを求めるプログラム
estimate_mgauss_param(int *data, int n, int c, double *mix_mean,
                      double *min_var, double *) {
  int i, j, k, iteration;
  bool endflag;
  //	int c;
  //	double data[65536];
  //	int n; //データ数

  double *alpha = new double[c];
  double *mean = new double[c];
  double *var = new double[c];
  double *alpha_dash = new double[c];
  double *mean_dash = new double[c];
  double *var_dash = new double[c];
  double phai;

  double max = -1.0e100;
  double min = 1.0e100;
  for (i = 0; i < n; i++) {
    if (max < data[i])
      max = data[i];
    else if (min > data[i])
      min = data[i];
  }
  //パラメータの初期化
  for (i = 0; i < c; i++) {
    alpha_dash[i] = 1.0 / c;
    mean_dash[i] = (max - min) * i / c + min;
    var_dash[i] = 0.05;
  }

  for (iteration = 0; iteration < 100; iteration++) {

    for (i = 0; i < c; i++) {
      alpha[i] = 0.0;
      mean[i] = 0.0;
      var[i] = 0.0;
      for (k = 0; k < n; k++) {
        phai = alpha_dash[i] * gauss(data[k], mean_dash[i], var_dash[i]) /
               mixtured_gauss(data[k], alpha_dash, mean_dash, var_dash, c);
        alpha[i] += phai;
        mean[i] += phai * data[k];
        var[i] += phai * sqr(data[k]);
      }
      mean[i] /= alpha[i];
      var[i] = var[i] / alpha[i] - sqr(mean[i]);
      alpha[i] /= n;
    }

    //終了判定
    endflag = true;
    for (i = 0; i < c; i++) {
      if (fabs(mean[i] - mean_dash[i]) > DDD) endflag = false;
      if (fabs(alpha[i] - alpha_dash[i]) > DDD) endflag = false;
      if (fabs(var[i] - var_dash[i]) > DDD) endflag = false;
    }
    if (endflag)
      break;
    else {
      for (i = 0; i < c; i++) {
        alpha_dash[i] = alpha[i];
        mean_dash[i] = mean[i];
        var_dash[i] = var[i];
      }
    }
  }

  //結果出力
  printf("%d\n", c);
  for (i = 0; i < c; i++) {
    printf("%lf %lf %lf\n", alpha[i], mean[i], var[i]);
  }

  FILE *fp2;
  if ((fp2 = fopen("mg_wave.txt", "at")) != NULL) {

    for (j = 0; j < 100; j++) {
      fprintf(fp2, "%lf,", j * 0.01);
    }
    fprintf(fp2, "\n");
    for (j = 0; j < 100; j++) {
      double mg = mixtured_gauss(j * 0.01, alpha, mean, var, c);
      fprintf(fp2, "%lf,", mg);
    }
    fprintf(fp2, "\n");
    fclose(fp2);
  }

  delete alpha;
  delete mean;
  delete var;
  delete alpha_dash;
  delete mean_dash;
  delete var_dash;
}

double mixtured_gauss(double x, double *alpha, double *mean, double *var,
                      int c) {
  int i;
  double retval;

  retval = 0;
  for (i = 0; i < c; i++) {
    retval += alpha[i] * gauss(x, mean[i], var[i]);
  }

  return retval;
}

double gauss(double x, double mean, double var) {
  double a = sqr(x - mean);
  double retval;

  retval = exp(a / (-2.0 * var));

  retval /= sqrt(2.0 * 3.141592 * var);
  return (retval);
}