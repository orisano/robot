#include "Expand.h"

/*--- expand --- 濃度を０から２５５の範囲に変換する ---------------------------
        image_in:	入力画像配列
        image_out:	出力画像配列
        fmax:		入力画像の濃度の最大値
        fmin:		入力画像の濃度の最小値
-----------------------------------------------------------------------------*/
void expand(unsigned char image_in[Y_SIZE][X_SIZE],
            unsigned char image_out[Y_SIZE][X_SIZE], int fmax, int fmin) {
  int i, j;
  float d;

  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      d = (float)255 / (float)(fmax - fmin) * ((int)image_in[i][j] - fmin);
      if (d > 255)
        image_out[i][j] = 255;
      else if (d < 0)
        image_out[i][j] = 0;
      else
        image_out[i][j] = (unsigned char)d;
    }
  }
}
