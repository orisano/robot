#ifndef __INCLUDE_MEDIAN_H__
#define __INCLUDE_MEDIAN_H__

#include "Params.h"

void median(unsigned char image_in[Y_SIZE][X_SIZE],
            unsigned char image_out[Y_SIZE][X_SIZE]);
#endif
