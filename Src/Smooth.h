#ifndef __INCLUDE_SMOOTH_H__
#define __INCLUDE_SMOOTH_H__

#include "Params.h"

void smooth(unsigned char image_in[Y_SIZE][X_SIZE],
            unsigned char image_out[Y_SIZE][X_SIZE]);

#endif
