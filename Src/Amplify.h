#ifndef __INCLUDE_AMPLIFY_H__
#define __INCLUDE_AMPLIFY_H__

#include "Params.h"

void amplify(unsigned char image_in[Y_SIZE][X_SIZE],
             unsigned char image_out[Y_SIZE][X_SIZE], int n);

#endif
