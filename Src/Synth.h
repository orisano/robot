#ifndef __INCLUDE_SYNTH_H__
#define __INCLUDE_SYNTH_H__

#include "Params.h"

void synth(unsigned char image_in1_r[Y_SIZE][X_SIZE],
           unsigned char image_in1_g[Y_SIZE][X_SIZE],
           unsigned char image_in1_b[Y_SIZE][X_SIZE],
           unsigned char image_in2_r[Y_SIZE][X_SIZE],
           unsigned char image_in2_g[Y_SIZE][X_SIZE],
           unsigned char image_in2_b[Y_SIZE][X_SIZE],
           unsigned char image_out_r[Y_SIZE][X_SIZE],
           unsigned char image_out_g[Y_SIZE][X_SIZE],
           unsigned char image_out_b[Y_SIZE][X_SIZE],
           unsigned char image_key[Y_SIZE][X_SIZE]);

#endif
