#include "Range.h"

/*--- range --- 画像の明るさの範囲を求める
------------------------------------
        image_in:	入力画像配列
        fmax:		入力画像の濃度の最大値
        fmin:		入力画像の濃度の最小値
-----------------------------------------------------------------------------*/
void range(unsigned char image_in[Y_SIZE][X_SIZE], int *fmax, int *fmin) {
  int i, j, nf;

  *fmax = 0;
  *fmin = 255;
  for (i = 0; i < Y_SIZE; i++) {
    for (j = 0; j < X_SIZE; j++) {
      nf = (int)image_in[i][j];
      if (nf > *fmax) *fmax = nf;
      if (nf < *fmin) *fmin = nf;
    }
  }
}
