# Microsoft Developer Studio Project File - Name="WinCImage" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** 編集しないでください **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=WinCImage - Win32 Release
!MESSAGE これは有効なﾒｲｸﾌｧｲﾙではありません。 このﾌﾟﾛｼﾞｪｸﾄをﾋﾞﾙﾄﾞするためには NMAKE を使用してください。
!MESSAGE [ﾒｲｸﾌｧｲﾙのｴｸｽﾎﾟｰﾄ] ｺﾏﾝﾄﾞを使用して実行してください
!MESSAGE 
!MESSAGE NMAKE /f "WinCImage.mak".
!MESSAGE 
!MESSAGE NMAKE の実行時に構成を指定できます
!MESSAGE ｺﾏﾝﾄﾞ ﾗｲﾝ上でﾏｸﾛの設定を定義します。例:
!MESSAGE 
!MESSAGE NMAKE /f "WinCImage.mak" CFG="WinCImage - Win32 Release"
!MESSAGE 
!MESSAGE 選択可能なﾋﾞﾙﾄﾞ ﾓｰﾄﾞ:
!MESSAGE 
!MESSAGE "WinCImage - Win32 Release" ("Win32 (x86) Application" 用)
!MESSAGE "WinCImage - Win32 Debug" ("Win32 (x86) Application" 用)
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "WinCImage - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\Release"
# PROP BASE Intermediate_Dir ".\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\Release"
# PROP Intermediate_Dir ".\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /W3 /GX /Zd /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "NDEBUG"
# ADD RSC /l 0x411 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ..\..\src\pgrflycapture.lib /nologo /subsystem:windows /debug /machine:I386

!ELSEIF  "$(CFG)" == "WinCImage - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Debug"
# PROP BASE Intermediate_Dir ".\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\Debug"
# PROP Intermediate_Dir ".\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\..\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "_DEBUG"
# ADD RSC /l 0x411 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ..\..\lib\pnmutils.lib strmiids.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "WinCImage - Win32 Release"
# Name "WinCImage - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=..\..\Src\Affine.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Amplify.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\cam_usb_std_x.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Colorbar.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\CSh.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Dilation.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Disk.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Erosion.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Event.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Expand.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Features.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\global.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Gradient.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\HardMask.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Hist2Im.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Histgram.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\HistImag.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\ImageCom.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\ImageTra0713.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Labeling.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Laplacia.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Masking.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Median.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Perspect.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Plane.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Range.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Rextract.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\RgbYc.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\RobotCmd.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Src\Rotation.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Scale.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\ScaleNea.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Sextract.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Shift.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\ShImage.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Smooth.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\SoftMask.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Ssynth.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Synth.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Template.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Thinning.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\Thresh.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\ThreshC.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\TranYsh.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\wait.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinCImage.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinCImage.rc
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinDialog.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinDisp.c
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinPrint.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=..\..\Src\Params.h
# End Source File
# Begin Source File

SOURCE=..\..\Src\Proto.h
# End Source File
# Begin Source File

SOURCE=..\..\Src\RobotCmd.h
# End Source File
# Begin Source File

SOURCE=..\..\Src\SystemCtrl.h

!IF  "$(CFG)" == "WinCImage - Win32 Release"

!ELSEIF  "$(CFG)" == "WinCImage - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Src\WinCImage.h
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinDialog.h
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinParams.h
# End Source File
# Begin Source File

SOURCE=..\..\Src\WinProto.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE="C:.\Program Files\OpenCV\bin\cxcore100.dll"
# End Source File
# Begin Source File

SOURCE="C:\Program Files\OpenCV\bin\highgui100.dll"
# End Source File
# Begin Source File

SOURCE=..\..\Src\SystemCtrl.dll
# End Source File
# Begin Source File

SOURCE="C:\Program Files\OpenCV\lib\cxcore.lib"
# End Source File
# Begin Source File

SOURCE="C:\Program Files\OpenCV\lib\highgui.lib"
# End Source File
# Begin Source File

SOURCE="C:\Program Files\OpenCV\lib\cv.lib"
# End Source File
# Begin Source File

SOURCE=..\..\Src\SystemCtrl.lib
# End Source File
# End Target
# End Project
